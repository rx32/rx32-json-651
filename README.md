# rx32-json-651

Problem about code coverage:

After I modified the default descriptor to public descriptor, I can achieve 100% code coverage.
But if I do not modify the descriptor, the class declaration would not be covered. I am still 
trying to find the reason.
