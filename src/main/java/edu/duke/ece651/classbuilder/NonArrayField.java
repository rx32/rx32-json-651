package edu.duke.ece651.classbuilder;

import org.json.JSONObject;

class NonArrayField extends Field {

  NonArrayField(JSONObject fieldJSON) {
    super(fieldJSON);
    this.type = fieldJSON.getString("type");
  }


}
