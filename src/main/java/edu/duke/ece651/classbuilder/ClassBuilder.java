package edu.duke.ece651.classbuilder;

import org.json.*;
import java.io.*;
import java.util.*;

public class ClassBuilder {

  private JSONObject root;
  private Map<String, ClassGenerator> classMap;
  private String packageDirectory;
  private String packageName;
  private ClassGenerator classGenerator;
  /**
   * constructor that takes a string as parameter
   *
   * @param jsonStr - json description string
   * @throws IllegalArgumentException - throw if the JSON is not valid, or does not conform to the input specification requirements
   */
  public ClassBuilder(String jsonStr) throws IllegalArgumentException, IOException {
    root = new JSONObject(jsonStr);
    packageName = root.optString("package");
    constructClasses();
  }
  /**
   * constructor that support InputStream as input
   *
   * @param inputStream - inputStream to read from to obtain the text of JSON description
   * @throws IOException - throw if an IO error occurs reading from the InputStream
   * @throws IllegalArgumentException - throw if the JSON is not valid, or does not conform to the input specification requirements
   */
  public ClassBuilder(InputStream inputStream) throws IOException {
    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
    StringBuilder sb = new StringBuilder();
    String line = null;
    while ((line = reader.readLine()) != null) {
      sb.append(line);
    }

    root = new JSONObject(sb.toString());
    packageName = root.optString("package");
    constructClasses();
  }

  private void constructClasses() {
    classMap = new LinkedHashMap<>();
    for (Object classObj : root.getJSONArray("classes")) {
      JSONObject classJSON = (JSONObject) classObj;
      String className = classJSON.getString("name");
      if (classMap.containsKey(className)) {
        throw new IllegalArgumentException("Duplicate class name: " + className);
      }
      classMap.put(className, new ClassGenerator(classJSON));
    }
  }

  /**
   * get all the class names
   *
   * @return the names of all classes that were described by the JSON description
   */
  public Collection<String> getClassNames() {
    return classMap.keySet();
  }

  /**
   * get generated source code according to class name
   *
   * @param className
   * @return
   */
  public String getSourceCode(String className) throws IllegalArgumentException {
    ClassGenerator classGenerator = classMap.get(className);
    if (classGenerator == null) {
      throw new IllegalArgumentException("Please input a valid class name!");
    }
    return classMap.get(className).getSouceCode();
  }

  /**
   * create one source file per class. These should all be made relative to the specified base path.
   * If the path are within a package, they will need to be in subdirectories of that path.
   * If they are not in a package then they will be made directly in that path
   *
   * @param basePath
   */
  void createAllClasses(String basePath) {

    packageDirectory = packageName == null ? "" : packageName.replaceAll("\\.", File.separator);
    new File(packageDirectory).mkdirs();
    basePath += File.separator + packageDirectory + File.separator;

    try (PrintWriter writer = new PrintWriter(basePath + "Deserializer" + ".java", "UTF-8")) {
      writer.println("package " + packageName + ";\n");
      writer.println(DeserializerBuilder.getSourceCode(classMap));
    } catch(IOException ioe) {
      System.err.println("Could not save your result to" + basePath);
      System.err.println("The reason was " + ioe);
      System.err.println("Please enter valid basePath!");
    }

    for (Map.Entry<String, ClassGenerator> entry : classMap.entrySet()) {
      String className = entry.getKey();
      try (PrintWriter writer = new PrintWriter(basePath + className + ".java", "UTF-8")) {
        ClassGenerator classGenerator = entry.getValue();
        writer.println("package " + packageName + ";\n");
        writer.println(classGenerator.getSouceCode());
      } catch (IOException ioe) {
        System.err.println("Could not save your result to" + basePath);
        System.err.println("The reason was " + ioe);
        System.err.println("Please enter valid basePath!");
      }
    }



  }

}
