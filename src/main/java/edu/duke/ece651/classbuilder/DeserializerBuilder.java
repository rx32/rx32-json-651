package edu.duke.ece651.classbuilder;

import java.util.Map;

final class DeserializerBuilder {
  // static method to generate deserializer
  static String getSourceCode(Map<String, ClassGenerator> classMap) {
    StringBuilder sb = new StringBuilder();
    sb.append("import org.json.*;\n");
    sb.append("import java.util.*;\n");
    sb.append("public class Deserializer {\n\n");
    for (Map.Entry<String, ClassGenerator> entry : classMap.entrySet()) {
      sb.append(entry.getValue().generateDeserializer());
    }
    sb.append("}\n");
    return sb.toString();
  }
}
