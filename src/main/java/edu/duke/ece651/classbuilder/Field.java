package edu.duke.ece651.classbuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;

abstract class Field {
  String name;
  String nameInitialLetterCapitalized;
  String type;

  protected final Set<String> wrapperSet = new HashSet<>(Arrays.asList("String", "Boolean", "Character", "Short", "Integer", "Long", "Byte", "Float", "Double"));

  protected static final Map<String, String> primitiveToWrapper = new HashMap<String, String>(){{
    put("char", "Character");
    put("short", "Short");
    put("int", "Integer");
    put("long", "Long");
    put("float", "Float");
    put("double", "Double");
    put("byte", "Byte");
    put("boolean", "Boolean");
    put("String", "String"); // not primitive but for convenience
  }};

  protected static final Map<String, String> primitiveToCast = new HashMap<String, String>(){{
    put("char", "(char)(int)");
    put("short", "(short)(int)");
    put("byte", "(byte)(int)");
    put("int", "(int)");
    put("long", "(Long)");
    put("float", "(Float)");
    put("double", "(Double)");
    put("boolean", "(Boolean)");
    put("String", "(String)"); // not primitive but for convenience
  }};

  protected static final Map<String, String> wrapperToCast = new HashMap<String, String>(){{
    put("Character", "(char)(int)");
    put("Short", "(short)(int)");
    put("Byte", "(byte)(int)");
    put("Integer", "(int)");
    put("Long", "(Long)");
    put("Float", "(Float)");
    put("Double", "(Double)");
    put("Boolean", "(Boolean)");
    put("String", "(String)"); // not primitive but for convenience
  }};

  protected static final Map<String, String> wrapperToPrimitive = new HashMap<String, String>(){{
    put("Character", "char");
    put("Short", "short");
    put("Byte", "byte");
    put("Integer", "int");
    put("Long", "long");
    put("Float", "float");
    put("Double", "double");
    put("Boolean", "boolean");
    put("String", "String"); // not primitive but for convenience
  }};


  Field(JSONObject fieldJSON) throws JSONException {
    name = fieldJSON.getString("name");
    nameInitialLetterCapitalized = name.substring(0, 1).toUpperCase() + name.substring(1);
  }

  // generate code for the nonarray field declaration.
  String generateField() {
    return String.format("private %s %s;\n", type, name);
  }

  // generate code for the nonarray field getter method.
  String generateGetter() {
    StringBuilder sb = new StringBuilder();
    sb.append(String.format("public %s get%s() {\n", type, nameInitialLetterCapitalized));
    sb.append(String.format("return this.%s;\n", name));
    sb.append("}\n\n");
    return sb.toString();
  }

  // generate code for the nonarray field setter method.
  String generateSetter() {
    StringBuilder sb = new StringBuilder();
    sb.append(String.format("public void set%s(%s val) {\n", nameInitialLetterCapitalized, type));
    sb.append(String.format("this.%s = val;\n", name));
    sb.append("}\n\n");
    return sb.toString();
  }

  // generate code for the nonarray field getter and setter method.
  String generateMethods() {
    StringBuilder sb = new StringBuilder();
    sb.append(generateGetter());
    sb.append(generateSetter());
    return sb.toString();
  }

  // generate code for the nonarray field toJSON method.
  String generateToJSON() {
    if (primitiveToWrapper.containsKey(type)) {
      StringBuilder sb = new StringBuilder();
      sb.append(String.format("JSONObject %sJSONObject = new JSONObject();\n", name));
      sb.append(String.format("%sJSONObject.put(\"%s\", %s);\n", name, name, name));
      sb.append(String.format("fieldList.add(%sJSONObject);\n\n", name));
      return sb.toString();
    } else {
      StringBuilder sb = new StringBuilder();
      sb.append(String.format("JSONObject %sJSONObject = %s.dfsSerializer(objToID);\n", name, name));
      sb.append(String.format("JSONObject %sJSONObjectWrapper = new JSONObject();\n", name));
      sb.append(String.format("%sJSONObjectWrapper.put(\"%s\", %sJSONObject);\n", name, name, name));
      sb.append(String.format("fieldList.add(%sJSONObjectWrapper);\n\n", name));
      return sb.toString();
    }
  }

  // generate code for the nonarray field deserializer method.
  String generateDeserializer(String className, int fieldIdx) {
    if (primitiveToWrapper.containsKey(type)) {
      return String.format("%s.set%s(%s(fieldList.getJSONObject(%d).get(\"%s\")));\n\n", className, nameInitialLetterCapitalized, primitiveToCast.get(type), fieldIdx, name);
    } else {
      return String.format("%s.set%s(dfs%sDeserializer(fieldList.getJSONObject(%d).getJSONObject(\"%s\"), IDToObj));\n\n", className, nameInitialLetterCapitalized, type, fieldIdx, name);
    }
  }

  // do not need to initialize nonarray field in constructor.
  String generateConstructerInitializer() {
    return "";
  }

}
