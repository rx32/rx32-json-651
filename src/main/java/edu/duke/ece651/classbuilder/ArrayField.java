package edu.duke.ece651.classbuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;

class ArrayField extends Field {

  String name;
  String getterType;
  String elementType;
  int dimension;
  ArrayField(JSONObject fieldJSON) throws JSONException {
    super(fieldJSON);
    dimension = 0;
    getNameAndType(fieldJSON);
  }

  // get the name and the type of the field
  private void getNameAndType(JSONObject fieldJSON) {
    StringBuilder sb = new StringBuilder();
    StringBuilder closeAngleBracket = new StringBuilder();
    name = fieldJSON.getString("name");
    JSONObject pre = fieldJSON.getJSONObject("type");
    JSONObject cur = pre;

    sb.append("ArrayList<");
    closeAngleBracket.append(">");
    cur = cur.optJSONObject("e");
    dimension++;

    while (cur != null) {
      sb.append("Collection<");
      closeAngleBracket.append(">");
      pre = cur;
      cur = cur.optJSONObject("e");
      dimension++;
    }

    elementType = pre.getString("e");
    elementType = primitiveToWrapper.getOrDefault(elementType, elementType);
    sb.append(elementType).append(closeAngleBracket);
    type = sb.toString();
    getterType = type.substring(10, type.length() - 1);
  }

  // generate getter method for array field
  String generateGetter() {
    StringBuilder sb = new StringBuilder();
    sb.append(String.format("public %s get%s(int index) {\n", wrapperToPrimitive.getOrDefault(getterType, getterType), nameInitialLetterCapitalized));
    sb.append(String.format("return this.%s.get(index);\n", name));
    sb.append("}\n\n");
    return sb.toString();
  }

  // generate setter method for array field
  String generateSetter() {
    StringBuilder sb = new StringBuilder();
    sb.append(String.format("public void set%s(int index, %s n) {\n", nameInitialLetterCapitalized, wrapperToPrimitive.getOrDefault(getterType, getterType)));
    sb.append(String.format("this.%s.add(n);\n", name));
    sb.append("}\n\n");
    return sb.toString();
  }

  // generate setter method for array field
  public String generateGetNumber() {
    StringBuilder sb = new StringBuilder();
    sb.append(String.format("public int num%s(){ \n", nameInitialLetterCapitalized));
    sb.append(String.format("return %s.size(); \n", name));
    sb.append("}\n\n");
    return sb.toString();
  }

  // generate setter method for array field
  public String generateAdder() {
    StringBuilder sb = new StringBuilder();
    sb.append(String.format("public void add%s(%s n) { \n", nameInitialLetterCapitalized, wrapperToPrimitive.getOrDefault(getterType, getterType)));
    sb.append(String.format("%s.add(n);\n", name));
    sb.append("} \n\n");
    return sb.toString();
  }

  @Override
  public String generateMethods() {
    StringBuilder sb = new StringBuilder();
    sb.append(super.generateMethods());
    sb.append(generateGetNumber());
    sb.append(generateAdder());
    return sb.toString();
  }

  @Override
  String generateToJSON() {
    StringBuilder firstPart = new StringBuilder();
    Deque<String> stack = new ArrayDeque<>();
    for (int i = 0; i < dimension; i++) {
      firstPart.append(String.format("JSONArray %sJsonArray%d = new JSONArray();\n", name, i));
      firstPart.append(String.format("for (%s l%d : %s) {\n", type.substring(10+11*i, type.length()-i-1), i, i == 0 ? name : "l" + (i-1)));
      stack.push("}\n");
      stack.push(i == dimension-1 ? toJSONInnermost() : String.format("%sJsonArray%d.put(%sJsonArray%d.length(), %sJsonArray%d);\n", name, i, name, i, name, i+1));
    }
    while (!stack.isEmpty()) {
      firstPart.append(stack.pop());
    }

    firstPart.append(String.format("JSONObject %sJSONObject = new JSONObject();\n", name));
    firstPart.append(String.format("%sJSONObject.put(\"%s\", %sJsonArray0);\n", name, name, name));
    firstPart.append(String.format("fieldList.add(%sJSONObject);\n\n", name));

    return firstPart.toString();
  }

  // helper function to generate the innermost for loop code
  String toJSONInnermost() {
    if (wrapperSet.contains(elementType)) {
      return String.format("%sJsonArray%d.put(%sJsonArray%d.length(), l%d);\n", name, dimension-1, name,  dimension-1, dimension-1);
    } else {
      return String.format("%sJsonArray%d.put(%sJsonArray%d.length(), l%d.dfsSerializer(objToID));\n", name, dimension-1, name, dimension-1, dimension-1);
    }
  }

  @Override
  String generateDeserializer(String className, int fieldIdx) {
    StringBuilder firstPart = new StringBuilder();
    Deque<String> stack = new ArrayDeque<>();
    firstPart.append(String.format("for (Object l0 : fieldList.getJSONObject(%d).getJSONArray(\"%s\")) {\n", fieldIdx, name));
    firstPart.append(dimension == 1 ? "" : String.format("%s c1 = new ArrayList<>();\n", type.substring(10, type.length()-1)));
    stack.push("}\n");
    stack.push(dimension == 1 ? String.format("%s.add%s(%s);\n", className, nameInitialLetterCapitalized, deserializerInnermost())
        : String.format("%s.add%s(c1);\n", className, nameInitialLetterCapitalized));
    for (int i = 1; i < dimension-1; i++) {
      firstPart.append(String.format("for (Object l%d : (JSONArray)l%d) {\n", i, i-1));
      firstPart.append(String.format("%s c%d = new ArrayList<>();\n", type.substring(10+11*i, type.length()-1-i), i+1));
      stack.push("}\n");
      stack.push(String.format("c%d.add(c%d);\n", i, i+1));
    }
    if (dimension > 1) {
      firstPart.append(String.format("for (Object l%d : (JSONArray)l%d) {\n", dimension-1, dimension-2));
      stack.push("}\n");
      stack.push(String.format("c%d.add(%s);\n", dimension-1, deserializerInnermost()));
    }

    while (!stack.isEmpty()) {
      firstPart.append(stack.pop());
    }

    return firstPart.toString();

  }

  // helper function to generalize the innermost for loop code
  private String deserializerInnermost() {
    if (wrapperSet.contains(elementType)) {
      return String.format("(%s)l%d", elementType, dimension-1);
    } else {
      return String.format("dfs%sDeserializer((JSONObject)l%d, IDToObj)",elementType, dimension-1);
    }
  }


  @Override
  String generateConstructerInitializer() {
    return String.format("this.%s = new ArrayList<>();\n", name);
  }

}
