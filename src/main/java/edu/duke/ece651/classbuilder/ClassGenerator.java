package edu.duke.ece651.classbuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;

class ClassGenerator {
  Map<String, Field> fieldMap;
  String name;
  String nameInitialLetterLowercase;

  ClassGenerator(JSONObject classJSON) throws JSONException {
    fieldMap = new LinkedHashMap<>();
    name = classJSON.getString("name");
    nameInitialLetterLowercase = name.substring(0, 1).toLowerCase() + name.substring(1);
    buildFieldMap(classJSON);
  }

  // create all the Field object and put them in the map
  void buildFieldMap(JSONObject classJSON) throws JSONException, IllegalArgumentException {
    for (Object fieldObj : classJSON.getJSONArray("fields")) {
      JSONObject fieldJSON = (JSONObject) fieldObj;
      String fieldName = fieldJSON.getString("name");
      if (fieldMap.containsKey(fieldName)) {
        throw new IllegalArgumentException("Duplicate field name: " + fieldName);
      }
      if (isArrayField(fieldJSON)) {
        fieldMap.put(fieldName, new ArrayField(fieldJSON));
      } else {
        fieldMap.put(fieldName, new NonArrayField(fieldJSON));
      }

    }
  }

  // check if the field is an array
  private boolean isArrayField(JSONObject fieldJSON) {
    return fieldJSON.get("type") instanceof JSONObject;
  }

  // get the source code of the class
  String getSouceCode() {
    StringBuilder sb = new StringBuilder();

    sb.append("import java.util.*;\n");
    sb.append("import org.json.*;\n\n");
    sb.append(String.format("public class %s { \n\n", name));

    sb.append(generateConstructor());

    for (Map.Entry<String, Field> entry : fieldMap.entrySet()) {
      sb.append(entry.getValue().generateField());
    }

    sb.append("\n");

    for (Map.Entry<String, Field> entry : fieldMap.entrySet()) {
      sb.append(entry.getValue().generateMethods());
    }

    sb.append(generateToJSON());

    sb.append("}");
    return sb.toString();
  }

  // generate constructor
  private String generateConstructor() {
    StringBuilder sb = new StringBuilder();
    sb.append(String.format("public %s() {\n", Character.toUpperCase(name.charAt(0)) + name.substring(1)));
    for (Map.Entry<String, Field> entry : fieldMap.entrySet()) {
      sb.append(entry.getValue().generateConstructerInitializer());
    }
    sb.append("}\n");
    return sb.toString();
  }

  // generate toJSON method code
  private String generateToJSON() {
    StringBuilder sb = new StringBuilder();
    sb.append("public JSONObject toJSON() {\n");
    sb.append("Map<Object, Integer> objToID = new HashMap<>();\n");
    sb.append("return dfsSerializer(objToID);\n");
    sb.append("}\n");
    sb.append("JSONObject dfsSerializer(Map<Object, Integer> objToID) {\n");
    sb.append("JSONObject jsonObject = new JSONObject();\n");
    sb.append("if (objToID.containsKey(this)) {\n");
    sb.append("jsonObject.put(\"ref\", objToID.get(this));\n");
    sb.append("} else {\n");
    sb.append("jsonObject.put(\"id\", objToID.size());\n");
    sb.append(String.format("jsonObject.put(\"type\", \"%s\");\n\n", name));
    sb.append("List<JSONObject> fieldList = new ArrayList<>();\n");
    sb.append("objToID.put(this, objToID.size());\n\n");
    for (Map.Entry<String, Field> entry : fieldMap.entrySet()) {
      sb.append(entry.getValue().generateToJSON());
    }
    sb.append("jsonObject.put(\"values\", fieldList);\n");
    sb.append("}\n");
    sb.append("return jsonObject;\n");
    sb.append("}\n");

    return sb.toString();
  }

  // generate deserializer method code
  public String generateDeserializer() {
    StringBuilder sb = new StringBuilder();
    sb.append(String.format("public static %s read%s(JSONObject js) throws JSONException {\n", name, name));
    sb.append(String.format("return dfs%sDeserializer(js, new HashMap<>());\n", name));
    sb.append("}\n\n");
    sb.append(String.format("private static %s dfs%sDeserializer(JSONObject js, Map<Integer, Object> IDToObj) {\n", name, name));
    sb.append("if (js.opt(\"ref\") == null) {\n");
    sb.append(String.format("%s %s = new %s();\n", name, nameInitialLetterLowercase, name));
    sb.append("int id = js.getInt(\"id\");\n");
    sb.append("JSONArray fieldList = js.getJSONArray(\"values\");\n");
    sb.append(String.format("IDToObj.put(id, %s);\n\n", nameInitialLetterLowercase));
    int idx = 0;
    for (Map.Entry<String, Field> entry : fieldMap.entrySet()) {
      sb.append(entry.getValue().generateDeserializer(nameInitialLetterLowercase, idx++));
    }
    sb.append(String.format("return %s;\n", nameInitialLetterLowercase));
    sb.append("} else {\n");
    sb.append(String.format("return (%s) IDToObj.get(js.getInt(\"ref\"));\n", name));
    sb.append("}\n");
    sb.append("}\n");

    return sb.toString();
  }

}
