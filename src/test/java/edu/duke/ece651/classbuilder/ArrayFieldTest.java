package edu.duke.ece651.classbuilder;

import org.json.*;
import java.util.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ArrayFieldTest {
  @Test
  public void testArrayPrimitive() {
    JSONObject fieldJSON = new JSONObject("{'name' : 'data', 'type': {'e': {'e' : {'e' : 'int'}}}}");
    Field field = new ArrayField(fieldJSON);
    JSONArray jsonArray = new JSONArray();
    JSONArray jsonArray1 = new JSONArray();
    jsonArray1.put(0, Arrays.asList(1,2,3,4,5,6));
    jsonArray.put(0, jsonArray1);
    System.out.println(ArrayField.primitiveToWrapper);
    System.out.println(jsonArray);
    System.out.println(field.generateField());
    System.out.println(field.generateMethods());
    System.out.println(field.generateToJSON());
    System.out.println(field.generateConstructerInitializer());
    System.out.println(field.generateDeserializer("Faculty", 1));
  }

  @Test
  public void testArrayClass() {
    JSONObject fieldJSON = new JSONObject("{'name' : 'data', 'type': {'e': {'e' : {'e' : 'Student'}}}}");
    Field field = new ArrayField(fieldJSON);

    System.out.println(field.generateField());
    System.out.println(field.generateMethods());
    System.out.println(field.generateToJSON());
    System.out.println(field.generateConstructerInitializer());
    System.out.println(field.generateDeserializer("Faculty", 1));
  }
}
