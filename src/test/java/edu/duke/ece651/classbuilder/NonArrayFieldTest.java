package edu.duke.ece651.classbuilder;

import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NonArrayFieldTest {
  @Test
  public void testNonArrayPrimitive() {
    JSONObject fieldJSON = new JSONObject("{'name' : 'numStudents', 'type' : 'int' }");
    Field field = new NonArrayField(fieldJSON);
    System.out.println(field.generateField());
    System.out.println(field.generateMethods());
    System.out.println(field.generateToJSON());
    System.out.println(field.generateConstructerInitializer());
    System.out.println(field.generateDeserializer("Faculty", 0));
  }

  @Test
  public void testNonArrayClass() {
    JSONObject fieldJSON = new JSONObject("{'name' : 'numStudents', 'type' : 'Num' }");
    Field field = new NonArrayField(fieldJSON);
    System.out.println(field.generateField());
    System.out.println(field.generateMethods());
    System.out.println(field.generateToJSON());
    System.out.println(field.generateConstructerInitializer());
    System.out.println(field.generateDeserializer("Faculty", 0));
  }
}