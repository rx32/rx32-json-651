package edu.duke.ece651.classbuilder;

import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;


class ClassBuilderTest {
  @Test
  public void test() throws IOException {

    ClassBuilder classBuilder = new ClassBuilder("{ 'package' : 'edu.duke.ece651.myawesomestuff', " +
        "'classes' :\n" +
        "  [\n" +
        "      {'name' : 'Course', 'fields' : [ {'name' : 'instructor', 'type' : 'Faculty'},\n" +
        "                                       {'name' : 'numStudents', 'type' : 'int' },\n" +
        "                                       {'name' : 'grades', 'type' : {'e': 'Grade'}}]},\n" +
        "      {'name' : 'Office',  'fields' : [ {'name' : 'occupant', 'type': 'Faculty'},\n" +
        "                                        {'name' : 'number', 'type': 'int'},\n" +
        "                                        {'name' : 'building' , 'type': 'String'}]},\n" +
        "      {'name' : 'Faculty', 'fields' : [ {'name' : 'name', 'type' : 'String' },\n" +
        "                                        {'name' : 'taught', 'type' : {'e': 'Course'}} ]},\n" +
        "      {'name' : 'Grade', 'fields' : [ {'name' : 'course', 'type' : 'Course'},\n" +
        "                                      {'name' : 'student', 'type' : 'String'},\n" +
        "                                      {'name' : 'grade', 'type': 'double'}]}\n" +
        "  ]\n" +
        "}\n");

    System.out.println(classBuilder.getSourceCode("Office"));
    classBuilder.createAllClasses("./");
    System.out.println(classBuilder.getClassNames());

  }

  @Test
  public void testPersonAndPet() throws IOException {
    ClassBuilder classBuilder = new ClassBuilder("{ 'package' : 'edu.duke.ece651.myawesomestuff', " +
        "'classes' :\n" +
        "  [   {'name' : 'Person', 'fields' : [ {'name' : 'name', 'type' : 'String'}, {'name' : 'age', 'type' : 'int' }, {'name' : 'pet', 'type' : { 'e': 'Pet'}} ] }," +
        "      {'name' : 'Pet', 'fields' : [ {'name' : 'name', 'type' : 'String'}, {'name' : 'owner', 'type' : 'Person'} ] } ] }");
    classBuilder.createAllClasses("./");
    System.out.println(classBuilder.getClassNames());
  }

  @Test
  public void testShape() throws IOException {
    InputStream inputStream = new FileInputStream("./inputs/shape.json");
    ClassBuilder classBuilder = new ClassBuilder(inputStream);
    classBuilder.createAllClasses("./");
  }

  @Test
  public void testTypeTest() throws IOException {
    System.out.println("Working Directory = " + System.getProperty("user.dir"));
    InputStream inputStream = new FileInputStream("./inputs/testType.json");
    ClassBuilder classBuilder = new ClassBuilder(inputStream);
    classBuilder.createAllClasses("./");
    System.out.println(classBuilder.getClassNames());
  }

  @Test
  public void testNonprimitive() throws IOException {
    System.out.println("Working Directory = " + System.getProperty("user.dir"));
    InputStream inputStream = new FileInputStream("./inputs/testNonprimitive.json");
    ClassBuilder classBuilder = new ClassBuilder(inputStream);
    classBuilder.createAllClasses("./");
    System.out.println(classBuilder.getClassNames());
  }

  @Test
  public void testSimple() throws IOException {
    System.out.println("Working Directory = " + System.getProperty("user.dir"));
    InputStream inputStream = new FileInputStream("./inputs/simple.json");
    ClassBuilder classBuilder = new ClassBuilder(inputStream);
    classBuilder.createAllClasses("./");
    System.out.println(classBuilder.getClassNames());
  }

  @Test
  public void testPrims() throws IOException {
    System.out.println("Working Directory = " + System.getProperty("user.dir"));
    InputStream inputStream = new FileInputStream("./inputs/prims.json");
    ClassBuilder classBuilder = new ClassBuilder(inputStream);
    classBuilder.createAllClasses("./");
    System.out.println(classBuilder.getClassNames());
  }

  @Test
  public void testZoo() throws IOException {
    System.out.println("Working Directory = " + System.getProperty("user.dir"));
    InputStream inputStream = new FileInputStream("./inputs/zoo.json");
    ClassBuilder classBuilder = new ClassBuilder(inputStream);
    classBuilder.createAllClasses("./");
    System.out.println(classBuilder.getClassNames());
  }

  @Test
  public void testCrazyArrays() throws IOException {
    System.out.println("Working Directory = " + System.getProperty("user.dir"));
    InputStream inputStream = new FileInputStream("./inputs/crazyArrays.json");
    ClassBuilder classBuilder = new ClassBuilder(inputStream);
    classBuilder.createAllClasses("./");
    System.out.println(classBuilder.getClassNames());

  }

  @Test
  public void testEmpty() throws IOException {
    System.out.println("Working Directory = " + System.getProperty("user.dir"));
    InputStream inputStream = new FileInputStream("./inputs/empty.json");
    ClassBuilder classBuilder = new ClassBuilder(inputStream);
    classBuilder.createAllClasses("./");
    System.out.println(classBuilder.getClassNames());

  }

  @Test
  public void testArr() throws IOException {
    System.out.println("Working Directory = " + System.getProperty("user.dir"));
    InputStream inputStream = new FileInputStream("./inputs/arr.json");
    ClassBuilder classBuilder = new ClassBuilder(inputStream);
    classBuilder.createAllClasses("./");
    System.out.println(classBuilder.getClassNames());
  }

  @Test
  public void testNameRef() throws IOException {
    System.out.println("Working Directory = " + System.getProperty("user.dir"));
    InputStream inputStream = new FileInputStream("./inputs/nameRef.json");
    ClassBuilder classBuilder = new ClassBuilder(inputStream);
    classBuilder.createAllClasses("./");
    System.out.println(classBuilder.getClassNames());
  }

  @Test
  public void testMdArr() throws IOException {
    System.out.println("Working Directory = " + System.getProperty("user.dir"));
    InputStream inputStream = new FileInputStream("./inputs/mdarr.json");
    ClassBuilder classBuilder = new ClassBuilder(inputStream);
    classBuilder.createAllClasses("./");
    System.out.println(classBuilder.getClassNames());
  }

  @Test
  public void testIncorrectPath() throws IOException {
    try {
      System.out.println("Working Directory = " + System.getProperty("user.dir"));
      InputStream inputStream = new FileInputStream("./inputs/nameRef.json");
      ClassBuilder classBuilder = new ClassBuilder(inputStream);
      classBuilder.createAllClasses("./dfasdgasdgasdg");
      System.out.println(classBuilder.getClassNames());
    } catch (Exception e) {
      System.out.println("e: " + e);
    }
  }

  @Test
  public void testIncorrectClassName() throws IOException {
    try {
      InputStream inputStream = new FileInputStream("./inputs/shape.json");
      ClassBuilder classBuilder = new ClassBuilder(inputStream);
      System.out.println(classBuilder.getSourceCode("sdfasdfasdf"));
    } catch (Exception e) {
      System.out.println("e: " + e);
    }
  }

  @Test
  public void testDuplicateClassName() throws IOException {
    try{
      InputStream inputStream = new FileInputStream("./inputs/testDuplicateClassName.json");
      ClassBuilder classBuilder = new ClassBuilder(inputStream);
    } catch (Exception e) {
      System.out.println("e: " + e);
    }
  }

  @Test
  public void testDuplicateFieldName() throws IOException {
    try {
      InputStream inputStream = new FileInputStream("./inputs/testDuplicateFieldName.json");
      ClassBuilder classBuilder = new ClassBuilder(inputStream);
      } catch (Exception e) {
        System.out.println("e: " + e);
    }
  }
}