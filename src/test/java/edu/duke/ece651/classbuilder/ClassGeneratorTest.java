package edu.duke.ece651.classbuilder;

import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;



class ClassGeneratorTest {
  @Test
  public void testPersonAndPet() {
    JSONObject classJSON = new JSONObject("{'name' : 'Person', 'fields' : [ {'name' : 'name', 'type' : 'String'},\n" +
        "                                       {'name' : 'age', 'type' : 'int' },\n" +
        "                                       {'name' : 'pet', 'type' : 'Pet'}]}");
    ClassGenerator classGenerator = new ClassGenerator(classJSON);
    System.out.println(classGenerator.getSouceCode());
    classJSON = new JSONObject("{'name' : 'Pet', 'fields' : [ {'name' : 'name', 'type' : 'String'},\n" +
        "                                       {'name' : 'owner', 'type' : 'Person'}]}");
    classGenerator = new ClassGenerator(classJSON);
    System.out.println(classGenerator.getSouceCode());
    System.out.println(classGenerator.generateDeserializer());

  }

  @Test
  public void testPersonAndPetArr() {
    JSONObject classJSON = new JSONObject("{'name' : 'Person', 'fields' : [ {'name' : 'name', 'type' : 'String'},\n" +
        "                                       {'name' : 'age', 'type' : 'int' },\n" +
        "                                       {'name' : 'pet', 'type' : {'e': 'Pet'}}]}");
    ClassGenerator classGenerator = new ClassGenerator(classJSON);
    System.out.println(classGenerator.getSouceCode());
    classJSON = new JSONObject("{'name' : 'Pet', 'fields' : [ {'name' : 'name', 'type' : 'String'},\n" +
        "                                       {'name' : 'owner', 'type' : 'Person'}]}");
    classGenerator = new ClassGenerator(classJSON);
    System.out.println(classGenerator.getSouceCode());
    System.out.println(classGenerator.generateDeserializer());
  }
}