package hwk1.testing.arr;

import java.util.*;
import org.json.*;

public class Course { 

public Course() {
this.grades = new ArrayList<>();
}
private Faculty instructor;
private int numStudents;
private ArrayList<Grade> grades;

public Faculty getInstructor() {
return this.instructor;
}

public void setInstructor(Faculty val) {
this.instructor = val;
}

public int getNumStudents() {
return this.numStudents;
}

public void setNumStudents(int val) {
this.numStudents = val;
}

public Grade getGrades(int index) {
return this.grades.get(index);
}

public void setGrades(int index, Grade n) {
this.grades.add(n);
}

public int numGrades(){ 
return grades.size(); 
}

public void addGrades(Grade n) { 
grades.add(n);
} 

public JSONObject toJSON() {
Map<Object, Integer> objToID = new HashMap<>();
return dfsSerializer(objToID);
}
JSONObject dfsSerializer(Map<Object, Integer> objToID) {
JSONObject jsonObject = new JSONObject();
if (objToID.containsKey(this)) {
jsonObject.put("ref", objToID.get(this));
} else {
jsonObject.put("id", objToID.size());
jsonObject.put("type", "Course");

List<JSONObject> fieldList = new ArrayList<>();
objToID.put(this, objToID.size());

JSONObject instructorJSONObject = instructor.dfsSerializer(objToID);
JSONObject instructorJSONObjectWrapper = new JSONObject();
instructorJSONObjectWrapper.put("instructor", instructorJSONObject);
fieldList.add(instructorJSONObjectWrapper);

JSONObject numStudentsJSONObject = new JSONObject();
numStudentsJSONObject.put("numStudents", numStudents);
fieldList.add(numStudentsJSONObject);

JSONArray gradesJsonArray0 = new JSONArray();
for (Grade l0 : grades) {
gradesJsonArray0.put(gradesJsonArray0.length(), l0.dfsSerializer(objToID));
}
JSONObject gradesJSONObject = new JSONObject();
gradesJSONObject.put("grades", gradesJsonArray0);
fieldList.add(gradesJSONObject);

jsonObject.put("values", fieldList);
}
return jsonObject;
}
}
