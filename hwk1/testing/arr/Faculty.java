package hwk1.testing.arr;

import java.util.*;
import org.json.*;

public class Faculty { 

public Faculty() {
this.taught = new ArrayList<>();
}
private String name;
private ArrayList<Course> taught;

public String getName() {
return this.name;
}

public void setName(String val) {
this.name = val;
}

public Course getTaught(int index) {
return this.taught.get(index);
}

public void setTaught(int index, Course n) {
this.taught.add(n);
}

public int numTaught(){ 
return taught.size(); 
}

public void addTaught(Course n) { 
taught.add(n);
} 

public JSONObject toJSON() {
Map<Object, Integer> objToID = new HashMap<>();
return dfsSerializer(objToID);
}
JSONObject dfsSerializer(Map<Object, Integer> objToID) {
JSONObject jsonObject = new JSONObject();
if (objToID.containsKey(this)) {
jsonObject.put("ref", objToID.get(this));
} else {
jsonObject.put("id", objToID.size());
jsonObject.put("type", "Faculty");

List<JSONObject> fieldList = new ArrayList<>();
objToID.put(this, objToID.size());

JSONObject nameJSONObject = new JSONObject();
nameJSONObject.put("name", name);
fieldList.add(nameJSONObject);

JSONArray taughtJsonArray0 = new JSONArray();
for (Course l0 : taught) {
taughtJsonArray0.put(taughtJsonArray0.length(), l0.dfsSerializer(objToID));
}
JSONObject taughtJSONObject = new JSONObject();
taughtJSONObject.put("taught", taughtJsonArray0);
fieldList.add(taughtJSONObject);

jsonObject.put("values", fieldList);
}
return jsonObject;
}
}
