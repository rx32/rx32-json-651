package hwk1.testing.prims;

import org.json.*;
import java.util.*;
public class Deserializer {

public static Prims readPrims(JSONObject js) throws JSONException {
return dfsPrimsDeserializer(js, new HashMap<>());
}

private static Prims dfsPrimsDeserializer(JSONObject js, Map<Integer, Object> IDToObj) {
if (js.opt("ref") == null) {
Prims prims = new Prims();
int id = js.getInt("id");
JSONArray fieldList = js.getJSONArray("values");
IDToObj.put(id, prims);

prims.setX((int)(fieldList.getJSONObject(0).get("x")));

prims.setIsAwesome((Boolean)(fieldList.getJSONObject(1).get("isAwesome")));

prims.setAteBits((byte)(int)(fieldList.getJSONObject(2).get("ateBits")));

prims.setBoat((Float)(fieldList.getJSONObject(3).get("boat")));

prims.setTrouble((Double)(fieldList.getJSONObject(4).get("trouble")));

prims.setIsntPronouncedLikeCare((char)(int)(fieldList.getJSONObject(5).get("isntPronouncedLikeCare")));

prims.setWaysAway((Long)(fieldList.getJSONObject(6).get("waysAway")));

prims.setStackOfPancakes((short)(int)(fieldList.getJSONObject(7).get("stackOfPancakes")));

return prims;
} else {
return (Prims) IDToObj.get(js.getInt("ref"));
}
}
}

