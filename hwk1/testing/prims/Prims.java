package hwk1.testing.prims;

import java.util.*;
import org.json.*;

public class Prims { 

public Prims() {
}
private int x;
private boolean isAwesome;
private byte ateBits;
private float boat;
private double trouble;
private char isntPronouncedLikeCare;
private long waysAway;
private short stackOfPancakes;

public int getX() {
return this.x;
}

public void setX(int val) {
this.x = val;
}

public boolean getIsAwesome() {
return this.isAwesome;
}

public void setIsAwesome(boolean val) {
this.isAwesome = val;
}

public byte getAteBits() {
return this.ateBits;
}

public void setAteBits(byte val) {
this.ateBits = val;
}

public float getBoat() {
return this.boat;
}

public void setBoat(float val) {
this.boat = val;
}

public double getTrouble() {
return this.trouble;
}

public void setTrouble(double val) {
this.trouble = val;
}

public char getIsntPronouncedLikeCare() {
return this.isntPronouncedLikeCare;
}

public void setIsntPronouncedLikeCare(char val) {
this.isntPronouncedLikeCare = val;
}

public long getWaysAway() {
return this.waysAway;
}

public void setWaysAway(long val) {
this.waysAway = val;
}

public short getStackOfPancakes() {
return this.stackOfPancakes;
}

public void setStackOfPancakes(short val) {
this.stackOfPancakes = val;
}

public JSONObject toJSON() {
Map<Object, Integer> objToID = new HashMap<>();
return dfsSerializer(objToID);
}
JSONObject dfsSerializer(Map<Object, Integer> objToID) {
JSONObject jsonObject = new JSONObject();
if (objToID.containsKey(this)) {
jsonObject.put("ref", objToID.get(this));
} else {
jsonObject.put("id", objToID.size());
jsonObject.put("type", "Prims");

List<JSONObject> fieldList = new ArrayList<>();
objToID.put(this, objToID.size());

JSONObject xJSONObject = new JSONObject();
xJSONObject.put("x", x);
fieldList.add(xJSONObject);

JSONObject isAwesomeJSONObject = new JSONObject();
isAwesomeJSONObject.put("isAwesome", isAwesome);
fieldList.add(isAwesomeJSONObject);

JSONObject ateBitsJSONObject = new JSONObject();
ateBitsJSONObject.put("ateBits", ateBits);
fieldList.add(ateBitsJSONObject);

JSONObject boatJSONObject = new JSONObject();
boatJSONObject.put("boat", boat);
fieldList.add(boatJSONObject);

JSONObject troubleJSONObject = new JSONObject();
troubleJSONObject.put("trouble", trouble);
fieldList.add(troubleJSONObject);

JSONObject isntPronouncedLikeCareJSONObject = new JSONObject();
isntPronouncedLikeCareJSONObject.put("isntPronouncedLikeCare", isntPronouncedLikeCare);
fieldList.add(isntPronouncedLikeCareJSONObject);

JSONObject waysAwayJSONObject = new JSONObject();
waysAwayJSONObject.put("waysAway", waysAway);
fieldList.add(waysAwayJSONObject);

JSONObject stackOfPancakesJSONObject = new JSONObject();
stackOfPancakesJSONObject.put("stackOfPancakes", stackOfPancakes);
fieldList.add(stackOfPancakesJSONObject);

jsonObject.put("values", fieldList);
}
return jsonObject;
}
}
