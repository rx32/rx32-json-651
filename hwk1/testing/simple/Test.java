package hwk1.testing.simple;

import java.util.*;
import org.json.*;

public class Test { 

public Test() {
}
private int x;

public int getX() {
return this.x;
}

public void setX(int val) {
this.x = val;
}

public JSONObject toJSON() {
Map<Object, Integer> objToID = new HashMap<>();
return dfsSerializer(objToID);
}
JSONObject dfsSerializer(Map<Object, Integer> objToID) {
JSONObject jsonObject = new JSONObject();
if (objToID.containsKey(this)) {
jsonObject.put("ref", objToID.get(this));
} else {
jsonObject.put("id", objToID.size());
jsonObject.put("type", "Test");

List<JSONObject> fieldList = new ArrayList<>();
objToID.put(this, objToID.size());

JSONObject xJSONObject = new JSONObject();
xJSONObject.put("x", x);
fieldList.add(xJSONObject);

jsonObject.put("values", fieldList);
}
return jsonObject;
}
}
