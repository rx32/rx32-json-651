package hwk1.testing.simple;

import org.json.*;
import java.util.*;
public class Deserializer {

public static Test readTest(JSONObject js) throws JSONException {
return dfsTestDeserializer(js, new HashMap<>());
}

private static Test dfsTestDeserializer(JSONObject js, Map<Integer, Object> IDToObj) {
if (js.opt("ref") == null) {
Test test = new Test();
int id = js.getInt("id");
JSONArray fieldList = js.getJSONArray("values");
IDToObj.put(id, test);

test.setX((int)(fieldList.getJSONObject(0).get("x")));

return test;
} else {
return (Test) IDToObj.get(js.getInt("ref"));
}
}
}

