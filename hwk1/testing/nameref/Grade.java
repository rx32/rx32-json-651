package hwk1.testing.nameref;

import java.util.*;
import org.json.*;

public class Grade { 

public Grade() {
}
private Course course;
private String student;
private double grade;

public Course getCourse() {
return this.course;
}

public void setCourse(Course val) {
this.course = val;
}

public String getStudent() {
return this.student;
}

public void setStudent(String val) {
this.student = val;
}

public double getGrade() {
return this.grade;
}

public void setGrade(double val) {
this.grade = val;
}

public JSONObject toJSON() {
Map<Object, Integer> objToID = new HashMap<>();
return dfsSerializer(objToID);
}
JSONObject dfsSerializer(Map<Object, Integer> objToID) {
JSONObject jsonObject = new JSONObject();
if (objToID.containsKey(this)) {
jsonObject.put("ref", objToID.get(this));
} else {
jsonObject.put("id", objToID.size());
jsonObject.put("type", "Grade");

List<JSONObject> fieldList = new ArrayList<>();
objToID.put(this, objToID.size());

JSONObject courseJSONObject = course.dfsSerializer(objToID);
JSONObject courseJSONObjectWrapper = new JSONObject();
courseJSONObjectWrapper.put("course", courseJSONObject);
fieldList.add(courseJSONObjectWrapper);

JSONObject studentJSONObject = new JSONObject();
studentJSONObject.put("student", student);
fieldList.add(studentJSONObject);

JSONObject gradeJSONObject = new JSONObject();
gradeJSONObject.put("grade", grade);
fieldList.add(gradeJSONObject);

jsonObject.put("values", fieldList);
}
return jsonObject;
}
}
