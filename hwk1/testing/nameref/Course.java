package hwk1.testing.nameref;

import java.util.*;
import org.json.*;

public class Course { 

public Course() {
}
private Faculty instructor;
private int numStudents;

public Faculty getInstructor() {
return this.instructor;
}

public void setInstructor(Faculty val) {
this.instructor = val;
}

public int getNumStudents() {
return this.numStudents;
}

public void setNumStudents(int val) {
this.numStudents = val;
}

public JSONObject toJSON() {
Map<Object, Integer> objToID = new HashMap<>();
return dfsSerializer(objToID);
}
JSONObject dfsSerializer(Map<Object, Integer> objToID) {
JSONObject jsonObject = new JSONObject();
if (objToID.containsKey(this)) {
jsonObject.put("ref", objToID.get(this));
} else {
jsonObject.put("id", objToID.size());
jsonObject.put("type", "Course");

List<JSONObject> fieldList = new ArrayList<>();
objToID.put(this, objToID.size());

JSONObject instructorJSONObject = instructor.dfsSerializer(objToID);
JSONObject instructorJSONObjectWrapper = new JSONObject();
instructorJSONObjectWrapper.put("instructor", instructorJSONObject);
fieldList.add(instructorJSONObjectWrapper);

JSONObject numStudentsJSONObject = new JSONObject();
numStudentsJSONObject.put("numStudents", numStudents);
fieldList.add(numStudentsJSONObject);

jsonObject.put("values", fieldList);
}
return jsonObject;
}
}
