package hwk1.testing.nameref;

import java.util.*;
import org.json.*;

public class Faculty { 

public Faculty() {
this.courses = new ArrayList<>();
}
private String name;
private ArrayList<Course> courses;

public String getName() {
return this.name;
}

public void setName(String val) {
this.name = val;
}

public Course getCourses(int index) {
return this.courses.get(index);
}

public void setCourses(int index, Course n) {
this.courses.add(n);
}

public int numCourses(){ 
return courses.size(); 
}

public void addCourses(Course n) { 
courses.add(n);
} 

public JSONObject toJSON() {
Map<Object, Integer> objToID = new HashMap<>();
return dfsSerializer(objToID);
}
JSONObject dfsSerializer(Map<Object, Integer> objToID) {
JSONObject jsonObject = new JSONObject();
if (objToID.containsKey(this)) {
jsonObject.put("ref", objToID.get(this));
} else {
jsonObject.put("id", objToID.size());
jsonObject.put("type", "Faculty");

List<JSONObject> fieldList = new ArrayList<>();
objToID.put(this, objToID.size());

JSONObject nameJSONObject = new JSONObject();
nameJSONObject.put("name", name);
fieldList.add(nameJSONObject);

JSONArray coursesJsonArray0 = new JSONArray();
for (Course l0 : courses) {
coursesJsonArray0.put(coursesJsonArray0.length(), l0.dfsSerializer(objToID));
}
JSONObject coursesJSONObject = new JSONObject();
coursesJSONObject.put("courses", coursesJsonArray0);
fieldList.add(coursesJSONObject);

jsonObject.put("values", fieldList);
}
return jsonObject;
}
}
