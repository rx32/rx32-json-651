package hwk1.testing.mdarr;

import org.json.*;
import java.util.*;
public class Deserializer {

public static Matrix3d readMatrix3d(JSONObject js) throws JSONException {
return dfsMatrix3dDeserializer(js, new HashMap<>());
}

private static Matrix3d dfsMatrix3dDeserializer(JSONObject js, Map<Integer, Object> IDToObj) {
if (js.opt("ref") == null) {
Matrix3d matrix3d = new Matrix3d();
int id = js.getInt("id");
JSONArray fieldList = js.getJSONArray("values");
IDToObj.put(id, matrix3d);

for (Object l0 : fieldList.getJSONObject(0).getJSONArray("data")) {
Collection<Collection<Integer>> c1 = new ArrayList<>();
for (Object l1 : (JSONArray)l0) {
Collection<Integer> c2 = new ArrayList<>();
for (Object l2 : (JSONArray)l1) {
c2.add((Integer)l2);
}
c1.add(c2);
}
matrix3d.addData(c1);
}
return matrix3d;
} else {
return (Matrix3d) IDToObj.get(js.getInt("ref"));
}
}
}

