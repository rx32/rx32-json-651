package hwk1.testing.mdarr;

import java.util.*;
import org.json.*;

public class Matrix3d { 

public Matrix3d() {
this.data = new ArrayList<>();
}
private ArrayList<Collection<Collection<Integer>>> data;

public Collection<Collection<Integer>> getData(int index) {
return this.data.get(index);
}

public void setData(int index, Collection<Collection<Integer>> n) {
this.data.add(n);
}

public int numData(){ 
return data.size(); 
}

public void addData(Collection<Collection<Integer>> n) { 
data.add(n);
} 

public JSONObject toJSON() {
Map<Object, Integer> objToID = new HashMap<>();
return dfsSerializer(objToID);
}
JSONObject dfsSerializer(Map<Object, Integer> objToID) {
JSONObject jsonObject = new JSONObject();
if (objToID.containsKey(this)) {
jsonObject.put("ref", objToID.get(this));
} else {
jsonObject.put("id", objToID.size());
jsonObject.put("type", "Matrix3d");

List<JSONObject> fieldList = new ArrayList<>();
objToID.put(this, objToID.size());

JSONArray dataJsonArray0 = new JSONArray();
for (Collection<Collection<Integer>> l0 : data) {
JSONArray dataJsonArray1 = new JSONArray();
for (Collection<Integer> l1 : l0) {
JSONArray dataJsonArray2 = new JSONArray();
for (Integer l2 : l1) {
dataJsonArray2.put(dataJsonArray2.length(), l2);
}
dataJsonArray1.put(dataJsonArray1.length(), dataJsonArray2);
}
dataJsonArray0.put(dataJsonArray0.length(), dataJsonArray1);
}
JSONObject dataJSONObject = new JSONObject();
dataJSONObject.put("data", dataJsonArray0);
fieldList.add(dataJSONObject);

jsonObject.put("values", fieldList);
}
return jsonObject;
}
}
