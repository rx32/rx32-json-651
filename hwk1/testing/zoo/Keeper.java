package hwk1.testing.zoo;

import java.util.*;
import org.json.*;

public class Keeper { 

public Keeper() {
this.myPens = new ArrayList<>();
}
private String firstName;
private String lastName;
private double salary;
private ArrayList<AnimalPen> myPens;

public String getFirstName() {
return this.firstName;
}

public void setFirstName(String val) {
this.firstName = val;
}

public String getLastName() {
return this.lastName;
}

public void setLastName(String val) {
this.lastName = val;
}

public double getSalary() {
return this.salary;
}

public void setSalary(double val) {
this.salary = val;
}

public AnimalPen getMyPens(int index) {
return this.myPens.get(index);
}

public void setMyPens(int index, AnimalPen n) {
this.myPens.add(n);
}

public int numMyPens(){ 
return myPens.size(); 
}

public void addMyPens(AnimalPen n) { 
myPens.add(n);
} 

public JSONObject toJSON() {
Map<Object, Integer> objToID = new HashMap<>();
return dfsSerializer(objToID);
}
JSONObject dfsSerializer(Map<Object, Integer> objToID) {
JSONObject jsonObject = new JSONObject();
if (objToID.containsKey(this)) {
jsonObject.put("ref", objToID.get(this));
} else {
jsonObject.put("id", objToID.size());
jsonObject.put("type", "Keeper");

List<JSONObject> fieldList = new ArrayList<>();
objToID.put(this, objToID.size());

JSONObject firstNameJSONObject = new JSONObject();
firstNameJSONObject.put("firstName", firstName);
fieldList.add(firstNameJSONObject);

JSONObject lastNameJSONObject = new JSONObject();
lastNameJSONObject.put("lastName", lastName);
fieldList.add(lastNameJSONObject);

JSONObject salaryJSONObject = new JSONObject();
salaryJSONObject.put("salary", salary);
fieldList.add(salaryJSONObject);

JSONArray myPensJsonArray0 = new JSONArray();
for (AnimalPen l0 : myPens) {
myPensJsonArray0.put(myPensJsonArray0.length(), l0.dfsSerializer(objToID));
}
JSONObject myPensJSONObject = new JSONObject();
myPensJSONObject.put("myPens", myPensJsonArray0);
fieldList.add(myPensJSONObject);

jsonObject.put("values", fieldList);
}
return jsonObject;
}
}
