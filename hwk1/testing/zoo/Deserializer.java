package hwk1.testing.zoo;

import org.json.*;
import java.util.*;
public class Deserializer {

public static Keeper readKeeper(JSONObject js) throws JSONException {
return dfsKeeperDeserializer(js, new HashMap<>());
}

private static Keeper dfsKeeperDeserializer(JSONObject js, Map<Integer, Object> IDToObj) {
if (js.opt("ref") == null) {
Keeper keeper = new Keeper();
int id = js.getInt("id");
JSONArray fieldList = js.getJSONArray("values");
IDToObj.put(id, keeper);

keeper.setFirstName((String)(fieldList.getJSONObject(0).get("firstName")));

keeper.setLastName((String)(fieldList.getJSONObject(1).get("lastName")));

keeper.setSalary((Double)(fieldList.getJSONObject(2).get("salary")));

for (Object l0 : fieldList.getJSONObject(3).getJSONArray("myPens")) {
keeper.addMyPens(dfsAnimalPenDeserializer((JSONObject)l0, IDToObj));
}
return keeper;
} else {
return (Keeper) IDToObj.get(js.getInt("ref"));
}
}
public static AnimalPen readAnimalPen(JSONObject js) throws JSONException {
return dfsAnimalPenDeserializer(js, new HashMap<>());
}

private static AnimalPen dfsAnimalPenDeserializer(JSONObject js, Map<Integer, Object> IDToObj) {
if (js.opt("ref") == null) {
AnimalPen animalPen = new AnimalPen();
int id = js.getInt("id");
JSONArray fieldList = js.getJSONArray("values");
IDToObj.put(id, animalPen);

animalPen.setXloc((Float)(fieldList.getJSONObject(0).get("xloc")));

animalPen.setYloc((Float)(fieldList.getJSONObject(1).get("yloc")));

animalPen.setWidth((Float)(fieldList.getJSONObject(2).get("width")));

animalPen.setLength((Float)(fieldList.getJSONObject(3).get("length")));

animalPen.setHeight((Float)(fieldList.getJSONObject(4).get("height")));

for (Object l0 : fieldList.getJSONObject(5).getJSONArray("animals")) {
animalPen.addAnimals(dfsAnimalDeserializer((JSONObject)l0, IDToObj));
}
animalPen.setSignText((String)(fieldList.getJSONObject(6).get("signText")));

return animalPen;
} else {
return (AnimalPen) IDToObj.get(js.getInt("ref"));
}
}
public static Animal readAnimal(JSONObject js) throws JSONException {
return dfsAnimalDeserializer(js, new HashMap<>());
}

private static Animal dfsAnimalDeserializer(JSONObject js, Map<Integer, Object> IDToObj) {
if (js.opt("ref") == null) {
Animal animal = new Animal();
int id = js.getInt("id");
JSONArray fieldList = js.getJSONArray("values");
IDToObj.put(id, animal);

animal.setMyName((String)(fieldList.getJSONObject(0).get("myName")));

animal.setSpecies((String)(fieldList.getJSONObject(1).get("species")));

animal.setAge((int)(fieldList.getJSONObject(2).get("age")));

for (Object l0 : fieldList.getJSONObject(3).getJSONArray("foodsByPreference")) {
Collection<Food> c1 = new ArrayList<>();
for (Object l1 : (JSONArray)l0) {
c1.add(dfsFoodDeserializer((JSONObject)l1, IDToObj));
}
animal.addFoodsByPreference(c1);
}
return animal;
} else {
return (Animal) IDToObj.get(js.getInt("ref"));
}
}
public static Food readFood(JSONObject js) throws JSONException {
return dfsFoodDeserializer(js, new HashMap<>());
}

private static Food dfsFoodDeserializer(JSONObject js, Map<Integer, Object> IDToObj) {
if (js.opt("ref") == null) {
Food food = new Food();
int id = js.getInt("id");
JSONArray fieldList = js.getJSONArray("values");
IDToObj.put(id, food);

food.setFoodName((String)(fieldList.getJSONObject(0).get("foodName")));

food.setIsMeat((Boolean)(fieldList.getJSONObject(1).get("isMeat")));

return food;
} else {
return (Food) IDToObj.get(js.getInt("ref"));
}
}
}

