package hwk1.testing.zoo;

import java.util.*;
import org.json.*;

public class Food { 

public Food() {
}
private String foodName;
private boolean isMeat;

public String getFoodName() {
return this.foodName;
}

public void setFoodName(String val) {
this.foodName = val;
}

public boolean getIsMeat() {
return this.isMeat;
}

public void setIsMeat(boolean val) {
this.isMeat = val;
}

public JSONObject toJSON() {
Map<Object, Integer> objToID = new HashMap<>();
return dfsSerializer(objToID);
}
JSONObject dfsSerializer(Map<Object, Integer> objToID) {
JSONObject jsonObject = new JSONObject();
if (objToID.containsKey(this)) {
jsonObject.put("ref", objToID.get(this));
} else {
jsonObject.put("id", objToID.size());
jsonObject.put("type", "Food");

List<JSONObject> fieldList = new ArrayList<>();
objToID.put(this, objToID.size());

JSONObject foodNameJSONObject = new JSONObject();
foodNameJSONObject.put("foodName", foodName);
fieldList.add(foodNameJSONObject);

JSONObject isMeatJSONObject = new JSONObject();
isMeatJSONObject.put("isMeat", isMeat);
fieldList.add(isMeatJSONObject);

jsonObject.put("values", fieldList);
}
return jsonObject;
}
}
