package hwk1.testing.zoo;

import java.util.*;
import org.json.*;

public class AnimalPen { 

public AnimalPen() {
this.animals = new ArrayList<>();
}
private float xloc;
private float yloc;
private float width;
private float length;
private float height;
private ArrayList<Animal> animals;
private String signText;

public float getXloc() {
return this.xloc;
}

public void setXloc(float val) {
this.xloc = val;
}

public float getYloc() {
return this.yloc;
}

public void setYloc(float val) {
this.yloc = val;
}

public float getWidth() {
return this.width;
}

public void setWidth(float val) {
this.width = val;
}

public float getLength() {
return this.length;
}

public void setLength(float val) {
this.length = val;
}

public float getHeight() {
return this.height;
}

public void setHeight(float val) {
this.height = val;
}

public Animal getAnimals(int index) {
return this.animals.get(index);
}

public void setAnimals(int index, Animal n) {
this.animals.add(n);
}

public int numAnimals(){ 
return animals.size(); 
}

public void addAnimals(Animal n) { 
animals.add(n);
} 

public String getSignText() {
return this.signText;
}

public void setSignText(String val) {
this.signText = val;
}

public JSONObject toJSON() {
Map<Object, Integer> objToID = new HashMap<>();
return dfsSerializer(objToID);
}
JSONObject dfsSerializer(Map<Object, Integer> objToID) {
JSONObject jsonObject = new JSONObject();
if (objToID.containsKey(this)) {
jsonObject.put("ref", objToID.get(this));
} else {
jsonObject.put("id", objToID.size());
jsonObject.put("type", "AnimalPen");

List<JSONObject> fieldList = new ArrayList<>();
objToID.put(this, objToID.size());

JSONObject xlocJSONObject = new JSONObject();
xlocJSONObject.put("xloc", xloc);
fieldList.add(xlocJSONObject);

JSONObject ylocJSONObject = new JSONObject();
ylocJSONObject.put("yloc", yloc);
fieldList.add(ylocJSONObject);

JSONObject widthJSONObject = new JSONObject();
widthJSONObject.put("width", width);
fieldList.add(widthJSONObject);

JSONObject lengthJSONObject = new JSONObject();
lengthJSONObject.put("length", length);
fieldList.add(lengthJSONObject);

JSONObject heightJSONObject = new JSONObject();
heightJSONObject.put("height", height);
fieldList.add(heightJSONObject);

JSONArray animalsJsonArray0 = new JSONArray();
for (Animal l0 : animals) {
animalsJsonArray0.put(animalsJsonArray0.length(), l0.dfsSerializer(objToID));
}
JSONObject animalsJSONObject = new JSONObject();
animalsJSONObject.put("animals", animalsJsonArray0);
fieldList.add(animalsJSONObject);

JSONObject signTextJSONObject = new JSONObject();
signTextJSONObject.put("signText", signText);
fieldList.add(signTextJSONObject);

jsonObject.put("values", fieldList);
}
return jsonObject;
}
}
