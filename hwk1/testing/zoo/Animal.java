package hwk1.testing.zoo;

import java.util.*;
import org.json.*;

public class Animal { 

public Animal() {
this.foodsByPreference = new ArrayList<>();
}
private String myName;
private String species;
private int age;
private ArrayList<Collection<Food>> foodsByPreference;

public String getMyName() {
return this.myName;
}

public void setMyName(String val) {
this.myName = val;
}

public String getSpecies() {
return this.species;
}

public void setSpecies(String val) {
this.species = val;
}

public int getAge() {
return this.age;
}

public void setAge(int val) {
this.age = val;
}

public Collection<Food> getFoodsByPreference(int index) {
return this.foodsByPreference.get(index);
}

public void setFoodsByPreference(int index, Collection<Food> n) {
this.foodsByPreference.add(n);
}

public int numFoodsByPreference(){ 
return foodsByPreference.size(); 
}

public void addFoodsByPreference(Collection<Food> n) { 
foodsByPreference.add(n);
} 

public JSONObject toJSON() {
Map<Object, Integer> objToID = new HashMap<>();
return dfsSerializer(objToID);
}
JSONObject dfsSerializer(Map<Object, Integer> objToID) {
JSONObject jsonObject = new JSONObject();
if (objToID.containsKey(this)) {
jsonObject.put("ref", objToID.get(this));
} else {
jsonObject.put("id", objToID.size());
jsonObject.put("type", "Animal");

List<JSONObject> fieldList = new ArrayList<>();
objToID.put(this, objToID.size());

JSONObject myNameJSONObject = new JSONObject();
myNameJSONObject.put("myName", myName);
fieldList.add(myNameJSONObject);

JSONObject speciesJSONObject = new JSONObject();
speciesJSONObject.put("species", species);
fieldList.add(speciesJSONObject);

JSONObject ageJSONObject = new JSONObject();
ageJSONObject.put("age", age);
fieldList.add(ageJSONObject);

JSONArray foodsByPreferenceJsonArray0 = new JSONArray();
for (Collection<Food> l0 : foodsByPreference) {
JSONArray foodsByPreferenceJsonArray1 = new JSONArray();
for (Food l1 : l0) {
foodsByPreferenceJsonArray1.put(foodsByPreferenceJsonArray1.length(), l1.dfsSerializer(objToID));
}
foodsByPreferenceJsonArray0.put(foodsByPreferenceJsonArray0.length(), foodsByPreferenceJsonArray1);
}
JSONObject foodsByPreferenceJSONObject = new JSONObject();
foodsByPreferenceJSONObject.put("foodsByPreference", foodsByPreferenceJsonArray0);
fieldList.add(foodsByPreferenceJSONObject);

jsonObject.put("values", fieldList);
}
return jsonObject;
}
}
