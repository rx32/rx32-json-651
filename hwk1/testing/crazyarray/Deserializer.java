package hwk1.testing.crazyarray;

import org.json.*;
import java.util.*;
public class Deserializer {

public static LotsOfArray readLotsOfArray(JSONObject js) throws JSONException {
return dfsLotsOfArrayDeserializer(js, new HashMap<>());
}

private static LotsOfArray dfsLotsOfArrayDeserializer(JSONObject js, Map<Integer, Object> IDToObj) {
if (js.opt("ref") == null) {
LotsOfArray lotsOfArray = new LotsOfArray();
int id = js.getInt("id");
JSONArray fieldList = js.getJSONArray("values");
IDToObj.put(id, lotsOfArray);

for (Object l0 : fieldList.getJSONObject(0).getJSONArray("x")) {
lotsOfArray.addX((Integer)l0);
}
for (Object l0 : fieldList.getJSONObject(1).getJSONArray("isAwesome")) {
Collection<Boolean> c1 = new ArrayList<>();
for (Object l1 : (JSONArray)l0) {
c1.add((Boolean)l1);
}
lotsOfArray.addIsAwesome(c1);
}
for (Object l0 : fieldList.getJSONObject(2).getJSONArray("ateBits")) {
Collection<Collection<Byte>> c1 = new ArrayList<>();
for (Object l1 : (JSONArray)l0) {
Collection<Byte> c2 = new ArrayList<>();
for (Object l2 : (JSONArray)l1) {
c2.add((Byte)l2);
}
c1.add(c2);
}
lotsOfArray.addAteBits(c1);
}
for (Object l0 : fieldList.getJSONObject(3).getJSONArray("boat")) {
Collection<Collection<Collection<Float>>> c1 = new ArrayList<>();
for (Object l1 : (JSONArray)l0) {
Collection<Collection<Float>> c2 = new ArrayList<>();
for (Object l2 : (JSONArray)l1) {
Collection<Float> c3 = new ArrayList<>();
for (Object l3 : (JSONArray)l2) {
c3.add((Float)l3);
}
c2.add(c3);
}
c1.add(c2);
}
lotsOfArray.addBoat(c1);
}
for (Object l0 : fieldList.getJSONObject(4).getJSONArray("trouble")) {
lotsOfArray.addTrouble((Double)l0);
}
for (Object l0 : fieldList.getJSONObject(5).getJSONArray("isntPronouncedLikeCare")) {
Collection<Character> c1 = new ArrayList<>();
for (Object l1 : (JSONArray)l0) {
c1.add((Character)l1);
}
lotsOfArray.addIsntPronouncedLikeCare(c1);
}
for (Object l0 : fieldList.getJSONObject(6).getJSONArray("waysAway")) {
lotsOfArray.addWaysAway((Long)l0);
}
for (Object l0 : fieldList.getJSONObject(7).getJSONArray("stackOfPancakes")) {
Collection<Short> c1 = new ArrayList<>();
for (Object l1 : (JSONArray)l0) {
c1.add((Short)l1);
}
lotsOfArray.addStackOfPancakes(c1);
}
return lotsOfArray;
} else {
return (LotsOfArray) IDToObj.get(js.getInt("ref"));
}
}
}

