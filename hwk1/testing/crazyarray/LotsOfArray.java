package hwk1.testing.crazyarray;

import java.util.*;
import org.json.*;

public class LotsOfArray { 

public LotsOfArray() {
this.x = new ArrayList<>();
this.isAwesome = new ArrayList<>();
this.ateBits = new ArrayList<>();
this.boat = new ArrayList<>();
this.trouble = new ArrayList<>();
this.isntPronouncedLikeCare = new ArrayList<>();
this.waysAway = new ArrayList<>();
this.stackOfPancakes = new ArrayList<>();
}
private ArrayList<Integer> x;
private ArrayList<Collection<Boolean>> isAwesome;
private ArrayList<Collection<Collection<Byte>>> ateBits;
private ArrayList<Collection<Collection<Collection<Float>>>> boat;
private ArrayList<Double> trouble;
private ArrayList<Collection<Character>> isntPronouncedLikeCare;
private ArrayList<Long> waysAway;
private ArrayList<Collection<Short>> stackOfPancakes;

public int getX(int index) {
return this.x.get(index);
}

public void setX(int index, int n) {
this.x.add(n);
}

public int numX(){ 
return x.size(); 
}

public void addX(int n) { 
x.add(n);
} 

public Collection<Boolean> getIsAwesome(int index) {
return this.isAwesome.get(index);
}

public void setIsAwesome(int index, Collection<Boolean> n) {
this.isAwesome.add(n);
}

public int numIsAwesome(){ 
return isAwesome.size(); 
}

public void addIsAwesome(Collection<Boolean> n) { 
isAwesome.add(n);
} 

public Collection<Collection<Byte>> getAteBits(int index) {
return this.ateBits.get(index);
}

public void setAteBits(int index, Collection<Collection<Byte>> n) {
this.ateBits.add(n);
}

public int numAteBits(){ 
return ateBits.size(); 
}

public void addAteBits(Collection<Collection<Byte>> n) { 
ateBits.add(n);
} 

public Collection<Collection<Collection<Float>>> getBoat(int index) {
return this.boat.get(index);
}

public void setBoat(int index, Collection<Collection<Collection<Float>>> n) {
this.boat.add(n);
}

public int numBoat(){ 
return boat.size(); 
}

public void addBoat(Collection<Collection<Collection<Float>>> n) { 
boat.add(n);
} 

public double getTrouble(int index) {
return this.trouble.get(index);
}

public void setTrouble(int index, double n) {
this.trouble.add(n);
}

public int numTrouble(){ 
return trouble.size(); 
}

public void addTrouble(double n) { 
trouble.add(n);
} 

public Collection<Character> getIsntPronouncedLikeCare(int index) {
return this.isntPronouncedLikeCare.get(index);
}

public void setIsntPronouncedLikeCare(int index, Collection<Character> n) {
this.isntPronouncedLikeCare.add(n);
}

public int numIsntPronouncedLikeCare(){ 
return isntPronouncedLikeCare.size(); 
}

public void addIsntPronouncedLikeCare(Collection<Character> n) { 
isntPronouncedLikeCare.add(n);
} 

public long getWaysAway(int index) {
return this.waysAway.get(index);
}

public void setWaysAway(int index, long n) {
this.waysAway.add(n);
}

public int numWaysAway(){ 
return waysAway.size(); 
}

public void addWaysAway(long n) { 
waysAway.add(n);
} 

public Collection<Short> getStackOfPancakes(int index) {
return this.stackOfPancakes.get(index);
}

public void setStackOfPancakes(int index, Collection<Short> n) {
this.stackOfPancakes.add(n);
}

public int numStackOfPancakes(){ 
return stackOfPancakes.size(); 
}

public void addStackOfPancakes(Collection<Short> n) { 
stackOfPancakes.add(n);
} 

public JSONObject toJSON() {
Map<Object, Integer> objToID = new HashMap<>();
return dfsSerializer(objToID);
}
JSONObject dfsSerializer(Map<Object, Integer> objToID) {
JSONObject jsonObject = new JSONObject();
if (objToID.containsKey(this)) {
jsonObject.put("ref", objToID.get(this));
} else {
jsonObject.put("id", objToID.size());
jsonObject.put("type", "LotsOfArray");

List<JSONObject> fieldList = new ArrayList<>();
objToID.put(this, objToID.size());

JSONArray xJsonArray0 = new JSONArray();
for (Integer l0 : x) {
xJsonArray0.put(xJsonArray0.length(), l0);
}
JSONObject xJSONObject = new JSONObject();
xJSONObject.put("x", xJsonArray0);
fieldList.add(xJSONObject);

JSONArray isAwesomeJsonArray0 = new JSONArray();
for (Collection<Boolean> l0 : isAwesome) {
JSONArray isAwesomeJsonArray1 = new JSONArray();
for (Boolean l1 : l0) {
isAwesomeJsonArray1.put(isAwesomeJsonArray1.length(), l1);
}
isAwesomeJsonArray0.put(isAwesomeJsonArray0.length(), isAwesomeJsonArray1);
}
JSONObject isAwesomeJSONObject = new JSONObject();
isAwesomeJSONObject.put("isAwesome", isAwesomeJsonArray0);
fieldList.add(isAwesomeJSONObject);

JSONArray ateBitsJsonArray0 = new JSONArray();
for (Collection<Collection<Byte>> l0 : ateBits) {
JSONArray ateBitsJsonArray1 = new JSONArray();
for (Collection<Byte> l1 : l0) {
JSONArray ateBitsJsonArray2 = new JSONArray();
for (Byte l2 : l1) {
ateBitsJsonArray2.put(ateBitsJsonArray2.length(), l2);
}
ateBitsJsonArray1.put(ateBitsJsonArray1.length(), ateBitsJsonArray2);
}
ateBitsJsonArray0.put(ateBitsJsonArray0.length(), ateBitsJsonArray1);
}
JSONObject ateBitsJSONObject = new JSONObject();
ateBitsJSONObject.put("ateBits", ateBitsJsonArray0);
fieldList.add(ateBitsJSONObject);

JSONArray boatJsonArray0 = new JSONArray();
for (Collection<Collection<Collection<Float>>> l0 : boat) {
JSONArray boatJsonArray1 = new JSONArray();
for (Collection<Collection<Float>> l1 : l0) {
JSONArray boatJsonArray2 = new JSONArray();
for (Collection<Float> l2 : l1) {
JSONArray boatJsonArray3 = new JSONArray();
for (Float l3 : l2) {
boatJsonArray3.put(boatJsonArray3.length(), l3);
}
boatJsonArray2.put(boatJsonArray2.length(), boatJsonArray3);
}
boatJsonArray1.put(boatJsonArray1.length(), boatJsonArray2);
}
boatJsonArray0.put(boatJsonArray0.length(), boatJsonArray1);
}
JSONObject boatJSONObject = new JSONObject();
boatJSONObject.put("boat", boatJsonArray0);
fieldList.add(boatJSONObject);

JSONArray troubleJsonArray0 = new JSONArray();
for (Double l0 : trouble) {
troubleJsonArray0.put(troubleJsonArray0.length(), l0);
}
JSONObject troubleJSONObject = new JSONObject();
troubleJSONObject.put("trouble", troubleJsonArray0);
fieldList.add(troubleJSONObject);

JSONArray isntPronouncedLikeCareJsonArray0 = new JSONArray();
for (Collection<Character> l0 : isntPronouncedLikeCare) {
JSONArray isntPronouncedLikeCareJsonArray1 = new JSONArray();
for (Character l1 : l0) {
isntPronouncedLikeCareJsonArray1.put(isntPronouncedLikeCareJsonArray1.length(), l1);
}
isntPronouncedLikeCareJsonArray0.put(isntPronouncedLikeCareJsonArray0.length(), isntPronouncedLikeCareJsonArray1);
}
JSONObject isntPronouncedLikeCareJSONObject = new JSONObject();
isntPronouncedLikeCareJSONObject.put("isntPronouncedLikeCare", isntPronouncedLikeCareJsonArray0);
fieldList.add(isntPronouncedLikeCareJSONObject);

JSONArray waysAwayJsonArray0 = new JSONArray();
for (Long l0 : waysAway) {
waysAwayJsonArray0.put(waysAwayJsonArray0.length(), l0);
}
JSONObject waysAwayJSONObject = new JSONObject();
waysAwayJSONObject.put("waysAway", waysAwayJsonArray0);
fieldList.add(waysAwayJSONObject);

JSONArray stackOfPancakesJsonArray0 = new JSONArray();
for (Collection<Short> l0 : stackOfPancakes) {
JSONArray stackOfPancakesJsonArray1 = new JSONArray();
for (Short l1 : l0) {
stackOfPancakesJsonArray1.put(stackOfPancakesJsonArray1.length(), l1);
}
stackOfPancakesJsonArray0.put(stackOfPancakesJsonArray0.length(), stackOfPancakesJsonArray1);
}
JSONObject stackOfPancakesJSONObject = new JSONObject();
stackOfPancakesJSONObject.put("stackOfPancakes", stackOfPancakesJsonArray0);
fieldList.add(stackOfPancakesJSONObject);

jsonObject.put("values", fieldList);
}
return jsonObject;
}
}
