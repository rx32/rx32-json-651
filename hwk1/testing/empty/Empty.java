package hwk1.testing.empty;

import java.util.*;
import org.json.*;

public class Empty { 

public Empty() {
}

public JSONObject toJSON() {
Map<Object, Integer> objToID = new HashMap<>();
return dfsSerializer(objToID);
}
JSONObject dfsSerializer(Map<Object, Integer> objToID) {
JSONObject jsonObject = new JSONObject();
if (objToID.containsKey(this)) {
jsonObject.put("ref", objToID.get(this));
} else {
jsonObject.put("id", objToID.size());
jsonObject.put("type", "Empty");

List<JSONObject> fieldList = new ArrayList<>();
objToID.put(this, objToID.size());

jsonObject.put("values", fieldList);
}
return jsonObject;
}
}
