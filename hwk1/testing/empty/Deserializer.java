package hwk1.testing.empty;

import org.json.*;
import java.util.*;
public class Deserializer {

public static Empty readEmpty(JSONObject js) throws JSONException {
return dfsEmptyDeserializer(js, new HashMap<>());
}

private static Empty dfsEmptyDeserializer(JSONObject js, Map<Integer, Object> IDToObj) {
if (js.opt("ref") == null) {
Empty empty = new Empty();
int id = js.getInt("id");
JSONArray fieldList = js.getJSONArray("values");
IDToObj.put(id, empty);

return empty;
} else {
return (Empty) IDToObj.get(js.getInt("ref"));
}
}
}

