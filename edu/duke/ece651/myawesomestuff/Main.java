class Main {
  public static void main(String[] args) {
    Person person1 = new Person();
    Pet pet1 = new Pet();
    Pet pet2 = new Pet();
    pet1.setName("Dodo");
    pet2.setName("Fido");
    pet1.setOwner(person1);
    pet2.setOwner(person1);
    person1.addPet(pet1);
    person1.addPet(pet2);
    person1.setAge(40);
    person1.setName("John");
    System.out.println(person1.toJSON());
    System.out.println(pet1.toJSON());
    System.out.println(pet2.toJSON());
    Person p2 = Deserializer.readPerson(person1.toJSON());
    System.out.println(p2.getAge());
    System.out.println(p2.getName());
    System.out.println(p2.getPet(0).getName());
    System.out.println(p2.getPet(0).getOwner() == p2.getPet(1).getOwner());
    System.out.println(p2.getPet(1).getName());
  }
}