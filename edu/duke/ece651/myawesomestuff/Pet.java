package edu.duke.ece651.myawesomestuff;

import java.util.*;
import org.json.*;

public class Pet { 

public Pet() {
}
private String name;
private Person owner;

public String getName() {
return this.name;
}

public void setName(String val) {
this.name = val;
}

public Person getOwner() {
return this.owner;
}

public void setOwner(Person val) {
this.owner = val;
}

public JSONObject toJSON() {
Map<Object, Integer> objToID = new HashMap<>();
return dfsSerializer(objToID);
}
JSONObject dfsSerializer(Map<Object, Integer> objToID) {
JSONObject jsonObject = new JSONObject();
if (objToID.containsKey(this)) {
jsonObject.put("ref", objToID.get(this));
} else {
jsonObject.put("id", objToID.size());
jsonObject.put("type", "Pet");

List<JSONObject> fieldList = new ArrayList<>();
objToID.put(this, objToID.size());

JSONObject nameJSONObject = new JSONObject();
nameJSONObject.put("name", name);
fieldList.add(nameJSONObject);

JSONObject ownerJSONObject = owner.dfsSerializer(objToID);
JSONObject ownerJSONObjectWrapper = new JSONObject();
ownerJSONObjectWrapper.put("owner", ownerJSONObject);
fieldList.add(ownerJSONObjectWrapper);

jsonObject.put("values", fieldList);
}
return jsonObject;
}
}
