package edu.duke.ece651.myawesomestuff;

import java.util.*;
import org.json.*;

public class Person { 

public Person() {
this.pet = new ArrayList<>();
}
private String name;
private int age;
private ArrayList<Pet> pet;

public String getName() {
return this.name;
}

public void setName(String val) {
this.name = val;
}

public int getAge() {
return this.age;
}

public void setAge(int val) {
this.age = val;
}

public Pet getPet(int index) {
return this.pet.get(index);
}

public void setPet(int index, Pet n) {
this.pet.add(n);
}

public int numPet(){ 
return pet.size(); 
}

public void addPet(Pet n) { 
pet.add(n);
} 

public JSONObject toJSON() {
Map<Object, Integer> objToID = new HashMap<>();
return dfsSerializer(objToID);
}
JSONObject dfsSerializer(Map<Object, Integer> objToID) {
JSONObject jsonObject = new JSONObject();
if (objToID.containsKey(this)) {
jsonObject.put("ref", objToID.get(this));
} else {
jsonObject.put("id", objToID.size());
jsonObject.put("type", "Person");

List<JSONObject> fieldList = new ArrayList<>();
objToID.put(this, objToID.size());

JSONObject nameJSONObject = new JSONObject();
nameJSONObject.put("name", name);
fieldList.add(nameJSONObject);

JSONObject ageJSONObject = new JSONObject();
ageJSONObject.put("age", age);
fieldList.add(ageJSONObject);

JSONArray petJsonArray0 = new JSONArray();
for (Pet l0 : pet) {
petJsonArray0.put(petJsonArray0.length(), l0.dfsSerializer(objToID));
}
JSONObject petJSONObject = new JSONObject();
petJSONObject.put("pet", petJsonArray0);
fieldList.add(petJSONObject);

jsonObject.put("values", fieldList);
}
return jsonObject;
}
}
