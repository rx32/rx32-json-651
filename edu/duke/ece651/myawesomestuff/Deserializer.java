package edu.duke.ece651.myawesomestuff;

import org.json.*;
import java.util.*;
public class Deserializer {

public static Course readCourse(JSONObject js) throws JSONException {
return dfsCourseDeserializer(js, new HashMap<>());
}

private static Course dfsCourseDeserializer(JSONObject js, Map<Integer, Object> IDToObj) {
if (js.opt("ref") == null) {
Course course = new Course();
int id = js.getInt("id");
JSONArray fieldList = js.getJSONArray("values");
IDToObj.put(id, course);

course.setInstructor(dfsFacultyDeserializer(fieldList.getJSONObject(0).getJSONObject("instructor"), IDToObj));

course.setNumStudents((int)(fieldList.getJSONObject(1).get("numStudents")));

for (Object l0 : fieldList.getJSONObject(2).getJSONArray("grades")) {
course.addGrades(dfsGradeDeserializer((JSONObject)l0, IDToObj));
}
return course;
} else {
return (Course) IDToObj.get(js.getInt("ref"));
}
}
public static Office readOffice(JSONObject js) throws JSONException {
return dfsOfficeDeserializer(js, new HashMap<>());
}

private static Office dfsOfficeDeserializer(JSONObject js, Map<Integer, Object> IDToObj) {
if (js.opt("ref") == null) {
Office office = new Office();
int id = js.getInt("id");
JSONArray fieldList = js.getJSONArray("values");
IDToObj.put(id, office);

office.setOccupant(dfsFacultyDeserializer(fieldList.getJSONObject(0).getJSONObject("occupant"), IDToObj));

office.setNumber((int)(fieldList.getJSONObject(1).get("number")));

office.setBuilding((String)(fieldList.getJSONObject(2).get("building")));

return office;
} else {
return (Office) IDToObj.get(js.getInt("ref"));
}
}
public static Faculty readFaculty(JSONObject js) throws JSONException {
return dfsFacultyDeserializer(js, new HashMap<>());
}

private static Faculty dfsFacultyDeserializer(JSONObject js, Map<Integer, Object> IDToObj) {
if (js.opt("ref") == null) {
Faculty faculty = new Faculty();
int id = js.getInt("id");
JSONArray fieldList = js.getJSONArray("values");
IDToObj.put(id, faculty);

faculty.setName((String)(fieldList.getJSONObject(0).get("name")));

for (Object l0 : fieldList.getJSONObject(1).getJSONArray("taught")) {
faculty.addTaught(dfsCourseDeserializer((JSONObject)l0, IDToObj));
}
return faculty;
} else {
return (Faculty) IDToObj.get(js.getInt("ref"));
}
}
public static Grade readGrade(JSONObject js) throws JSONException {
return dfsGradeDeserializer(js, new HashMap<>());
}

private static Grade dfsGradeDeserializer(JSONObject js, Map<Integer, Object> IDToObj) {
if (js.opt("ref") == null) {
Grade grade = new Grade();
int id = js.getInt("id");
JSONArray fieldList = js.getJSONArray("values");
IDToObj.put(id, grade);

grade.setCourse(dfsCourseDeserializer(fieldList.getJSONObject(0).getJSONObject("course"), IDToObj));

grade.setStudent((String)(fieldList.getJSONObject(1).get("student")));

grade.setGrade((Double)(fieldList.getJSONObject(2).get("grade")));

return grade;
} else {
return (Grade) IDToObj.get(js.getInt("ref"));
}
}
}

