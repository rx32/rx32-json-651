package edu.duke.ece651.myawesomestuff;

import java.util.*;
import org.json.*;

public class Office { 

public Office() {
}
private Faculty occupant;
private int number;
private String building;

public Faculty getOccupant() {
return this.occupant;
}

public void setOccupant(Faculty val) {
this.occupant = val;
}

public int getNumber() {
return this.number;
}

public void setNumber(int val) {
this.number = val;
}

public String getBuilding() {
return this.building;
}

public void setBuilding(String val) {
this.building = val;
}

public JSONObject toJSON() {
Map<Object, Integer> objToID = new HashMap<>();
return dfsSerializer(objToID);
}
JSONObject dfsSerializer(Map<Object, Integer> objToID) {
JSONObject jsonObject = new JSONObject();
if (objToID.containsKey(this)) {
jsonObject.put("ref", objToID.get(this));
} else {
jsonObject.put("id", objToID.size());
jsonObject.put("type", "Office");

List<JSONObject> fieldList = new ArrayList<>();
objToID.put(this, objToID.size());

JSONObject occupantJSONObject = occupant.dfsSerializer(objToID);
JSONObject occupantJSONObjectWrapper = new JSONObject();
occupantJSONObjectWrapper.put("occupant", occupantJSONObject);
fieldList.add(occupantJSONObjectWrapper);

JSONObject numberJSONObject = new JSONObject();
numberJSONObject.put("number", number);
fieldList.add(numberJSONObject);

JSONObject buildingJSONObject = new JSONObject();
buildingJSONObject.put("building", building);
fieldList.add(buildingJSONObject);

jsonObject.put("values", fieldList);
}
return jsonObject;
}
}
