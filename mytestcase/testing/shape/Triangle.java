package mytestcase.testing.shape;

import java.util.*;
import org.json.*;

public class Triangle { 

public Triangle() {
this.innerShape = new ArrayList<>();
}
private Color color;
private Shape shape;
private int num;
private ArrayList<Collection<shape>> innerShape;

public Color getColor() {
return this.color;
}

public void setColor(Color val) {
this.color = val;
}

public Shape getShape() {
return this.shape;
}

public void setShape(Shape val) {
this.shape = val;
}

public int getNum() {
return this.num;
}

public void setNum(int val) {
this.num = val;
}

public Collection<shape> getInnerShape(int index) {
return this.innerShape.get(index);
}

public void setInnerShape(int index, Collection<shape> n) {
this.innerShape.add(n);
}

public int numInnerShape(){ 
return innerShape.size(); 
}

public void addInnerShape(Collection<shape> n) { 
innerShape.add(n);
} 

public JSONObject toJSON() {
Map<Object, Integer> objToID = new HashMap<>();
return dfsSerializer(objToID);
}
JSONObject dfsSerializer(Map<Object, Integer> objToID) {
JSONObject jsonObject = new JSONObject();
if (objToID.containsKey(this)) {
jsonObject.put("ref", objToID.get(this));
} else {
jsonObject.put("id", objToID.size());
jsonObject.put("type", "Triangle");

List<JSONObject> fieldList = new ArrayList<>();
objToID.put(this, objToID.size());

JSONObject colorJSONObject = color.dfsSerializer(objToID);
JSONObject colorJSONObjectWrapper = new JSONObject();
colorJSONObjectWrapper.put("color", colorJSONObject);
fieldList.add(colorJSONObjectWrapper);

JSONObject shapeJSONObject = shape.dfsSerializer(objToID);
JSONObject shapeJSONObjectWrapper = new JSONObject();
shapeJSONObjectWrapper.put("shape", shapeJSONObject);
fieldList.add(shapeJSONObjectWrapper);

JSONObject numJSONObject = new JSONObject();
numJSONObject.put("num", num);
fieldList.add(numJSONObject);

JSONArray innerShapeJsonArray0 = new JSONArray();
for (Collection<shape> l0 : innerShape) {
JSONArray innerShapeJsonArray1 = new JSONArray();
for (shape l1 : l0) {
innerShapeJsonArray1.put(innerShapeJsonArray1.length(), l1.dfsSerializer(objToID));
}
innerShapeJsonArray0.put(innerShapeJsonArray0.length(), innerShapeJsonArray1);
}
JSONObject innerShapeJSONObject = new JSONObject();
innerShapeJSONObject.put("innerShape", innerShapeJsonArray0);
fieldList.add(innerShapeJSONObject);

jsonObject.put("values", fieldList);
}
return jsonObject;
}
}
