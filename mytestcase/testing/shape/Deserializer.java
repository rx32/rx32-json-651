package mytestcase.testing.shape;

import org.json.*;
import java.util.*;
public class Deserializer {

public static Triangle readTriangle(JSONObject js) throws JSONException {
return dfsTriangleDeserializer(js, new HashMap<>());
}

private static Triangle dfsTriangleDeserializer(JSONObject js, Map<Integer, Object> IDToObj) {
if (js.opt("ref") == null) {
Triangle triangle = new Triangle();
int id = js.getInt("id");
JSONArray fieldList = js.getJSONArray("values");
IDToObj.put(id, triangle);

triangle.setColor(dfsColorDeserializer(fieldList.getJSONObject(0).getJSONObject("color"), IDToObj));

triangle.setShape(dfsShapeDeserializer(fieldList.getJSONObject(1).getJSONObject("shape"), IDToObj));

triangle.setNum((int)(fieldList.getJSONObject(2).get("num")));

for (Object l0 : fieldList.getJSONObject(3).getJSONArray("innerShape")) {
Collection<shape> c1 = new ArrayList<>();
for (Object l1 : (JSONArray)l0) {
c1.add(dfsshapeDeserializer((JSONObject)l1, IDToObj));
}
triangle.addInnerShape(c1);
}
return triangle;
} else {
return (Triangle) IDToObj.get(js.getInt("ref"));
}
}
public static Circle readCircle(JSONObject js) throws JSONException {
return dfsCircleDeserializer(js, new HashMap<>());
}

private static Circle dfsCircleDeserializer(JSONObject js, Map<Integer, Object> IDToObj) {
if (js.opt("ref") == null) {
Circle circle = new Circle();
int id = js.getInt("id");
JSONArray fieldList = js.getJSONArray("values");
IDToObj.put(id, circle);

circle.setColor(dfsColorDeserializer(fieldList.getJSONObject(0).getJSONObject("color"), IDToObj));

circle.setShape(dfsShapeDeserializer(fieldList.getJSONObject(1).getJSONObject("shape"), IDToObj));

circle.setNum((int)(fieldList.getJSONObject(2).get("num")));

for (Object l0 : fieldList.getJSONObject(3).getJSONArray("innerShape")) {
Collection<Collection<shape>> c1 = new ArrayList<>();
for (Object l1 : (JSONArray)l0) {
Collection<shape> c2 = new ArrayList<>();
for (Object l2 : (JSONArray)l1) {
c2.add(dfsshapeDeserializer((JSONObject)l2, IDToObj));
}
c1.add(c2);
}
circle.addInnerShape(c1);
}
return circle;
} else {
return (Circle) IDToObj.get(js.getInt("ref"));
}
}
public static Rectangle readRectangle(JSONObject js) throws JSONException {
return dfsRectangleDeserializer(js, new HashMap<>());
}

private static Rectangle dfsRectangleDeserializer(JSONObject js, Map<Integer, Object> IDToObj) {
if (js.opt("ref") == null) {
Rectangle rectangle = new Rectangle();
int id = js.getInt("id");
JSONArray fieldList = js.getJSONArray("values");
IDToObj.put(id, rectangle);

rectangle.setColor(dfsColorDeserializer(fieldList.getJSONObject(0).getJSONObject("color"), IDToObj));

rectangle.setShape(dfsShapeDeserializer(fieldList.getJSONObject(1).getJSONObject("shape"), IDToObj));

rectangle.setNum((int)(fieldList.getJSONObject(2).get("num")));

for (Object l0 : fieldList.getJSONObject(3).getJSONArray("innerShape")) {
Collection<shape> c1 = new ArrayList<>();
for (Object l1 : (JSONArray)l0) {
c1.add(dfsshapeDeserializer((JSONObject)l1, IDToObj));
}
rectangle.addInnerShape(c1);
}
return rectangle;
} else {
return (Rectangle) IDToObj.get(js.getInt("ref"));
}
}
}

