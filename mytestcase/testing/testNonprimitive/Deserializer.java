package mytestcase.testing.testNonprimitive;

import org.json.*;
import java.util.*;
public class Deserializer {

public static testType readtestType(JSONObject js) throws JSONException {
return dfstestTypeDeserializer(js, new HashMap<>());
}

private static testType dfstestTypeDeserializer(JSONObject js, Map<Integer, Object> IDToObj) {
if (js.opt("ref") == null) {
testType testType = new testType();
int id = js.getInt("id");
JSONArray fieldList = js.getJSONArray("values");
IDToObj.put(id, testType);

testType.setAType(dfsADeserializer(fieldList.getJSONObject(0).getJSONObject("AType"), IDToObj));

testType.setBType(dfsBDeserializer(fieldList.getJSONObject(1).getJSONObject("BType"), IDToObj));

testType.setCType(dfsCDeserializer(fieldList.getJSONObject(2).getJSONObject("CType"), IDToObj));

testType.setDDType(dfsDDDeserializer(fieldList.getJSONObject(3).getJSONObject("DDType"), IDToObj));

testType.setEEEType(dfsEEEDeserializer(fieldList.getJSONObject(4).getJSONObject("EEEType"), IDToObj));

testType.setFFType(dfsFFDeserializer(fieldList.getJSONObject(5).getJSONObject("FFType"), IDToObj));

testType.setGType(dfsGDeserializer(fieldList.getJSONObject(6).getJSONObject("GType"), IDToObj));

testType.setHType(dfsHDeserializer(fieldList.getJSONObject(7).getJSONObject("HType"), IDToObj));

for (Object l0 : fieldList.getJSONObject(8).getJSONArray("IIIIIIArr")) {
Collection<IIIIII> c1 = new ArrayList<>();
for (Object l1 : (JSONArray)l0) {
c1.add(dfsIIIIIIDeserializer((JSONObject)l1, IDToObj));
}
testType.addIIIIIIArr(c1);
}
for (Object l0 : fieldList.getJSONObject(9).getJSONArray("JArr")) {
Collection<J> c1 = new ArrayList<>();
for (Object l1 : (JSONArray)l0) {
c1.add(dfsJDeserializer((JSONObject)l1, IDToObj));
}
testType.addJArr(c1);
}
for (Object l0 : fieldList.getJSONObject(10).getJSONArray("KArr")) {
Collection<K> c1 = new ArrayList<>();
for (Object l1 : (JSONArray)l0) {
c1.add(dfsKDeserializer((JSONObject)l1, IDToObj));
}
testType.addKArr(c1);
}
for (Object l0 : fieldList.getJSONObject(11).getJSONArray("LArr")) {
Collection<L> c1 = new ArrayList<>();
for (Object l1 : (JSONArray)l0) {
c1.add(dfsLDeserializer((JSONObject)l1, IDToObj));
}
testType.addLArr(c1);
}
for (Object l0 : fieldList.getJSONObject(12).getJSONArray("MArr")) {
Collection<Collection<M>> c1 = new ArrayList<>();
for (Object l1 : (JSONArray)l0) {
Collection<M> c2 = new ArrayList<>();
for (Object l2 : (JSONArray)l1) {
c2.add(dfsMDeserializer((JSONObject)l2, IDToObj));
}
c1.add(c2);
}
testType.addMArr(c1);
}
for (Object l0 : fieldList.getJSONObject(13).getJSONArray("NArr")) {
testType.addNArr(dfsNDeserializer((JSONObject)l0, IDToObj));
}
for (Object l0 : fieldList.getJSONObject(14).getJSONArray("OArr")) {
Collection<Collection<Collection<O>>> c1 = new ArrayList<>();
for (Object l1 : (JSONArray)l0) {
Collection<Collection<O>> c2 = new ArrayList<>();
for (Object l2 : (JSONArray)l1) {
Collection<O> c3 = new ArrayList<>();
for (Object l3 : (JSONArray)l2) {
c3.add(dfsODeserializer((JSONObject)l3, IDToObj));
}
c2.add(c3);
}
c1.add(c2);
}
testType.addOArr(c1);
}
for (Object l0 : fieldList.getJSONObject(15).getJSONArray("PPPPPPPPPPPPPPArr")) {
Collection<PPPPPPPPPPPPPP> c1 = new ArrayList<>();
for (Object l1 : (JSONArray)l0) {
c1.add(dfsPPPPPPPPPPPPPPDeserializer((JSONObject)l1, IDToObj));
}
testType.addPPPPPPPPPPPPPPArr(c1);
}
return testType;
} else {
return (testType) IDToObj.get(js.getInt("ref"));
}
}
}

