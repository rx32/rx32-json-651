package mytestcase.testing.testNonprimitive;

import java.util.*;
import org.json.*;

public class testType { 

public TestType() {
this.IIIIIIArr = new ArrayList<>();
this.JArr = new ArrayList<>();
this.KArr = new ArrayList<>();
this.LArr = new ArrayList<>();
this.MArr = new ArrayList<>();
this.NArr = new ArrayList<>();
this.OArr = new ArrayList<>();
this.PPPPPPPPPPPPPPArr = new ArrayList<>();
}
private A AType;
private B BType;
private C CType;
private DD DDType;
private EEE EEEType;
private FF FFType;
private G GType;
private H HType;
private ArrayList<Collection<IIIIII>> IIIIIIArr;
private ArrayList<Collection<J>> JArr;
private ArrayList<Collection<K>> KArr;
private ArrayList<Collection<L>> LArr;
private ArrayList<Collection<Collection<M>>> MArr;
private ArrayList<N> NArr;
private ArrayList<Collection<Collection<Collection<O>>>> OArr;
private ArrayList<Collection<PPPPPPPPPPPPPP>> PPPPPPPPPPPPPPArr;

public A getAType() {
return this.AType;
}

public void setAType(A val) {
this.AType = val;
}

public B getBType() {
return this.BType;
}

public void setBType(B val) {
this.BType = val;
}

public C getCType() {
return this.CType;
}

public void setCType(C val) {
this.CType = val;
}

public DD getDDType() {
return this.DDType;
}

public void setDDType(DD val) {
this.DDType = val;
}

public EEE getEEEType() {
return this.EEEType;
}

public void setEEEType(EEE val) {
this.EEEType = val;
}

public FF getFFType() {
return this.FFType;
}

public void setFFType(FF val) {
this.FFType = val;
}

public G getGType() {
return this.GType;
}

public void setGType(G val) {
this.GType = val;
}

public H getHType() {
return this.HType;
}

public void setHType(H val) {
this.HType = val;
}

public Collection<IIIIII> getIIIIIIArr(int index) {
return this.IIIIIIArr.get(index);
}

public void setIIIIIIArr(int index, Collection<IIIIII> n) {
this.IIIIIIArr.add(n);
}

public int numIIIIIIArr(){ 
return IIIIIIArr.size(); 
}

public void addIIIIIIArr(Collection<IIIIII> n) { 
IIIIIIArr.add(n);
} 

public Collection<J> getJArr(int index) {
return this.JArr.get(index);
}

public void setJArr(int index, Collection<J> n) {
this.JArr.add(n);
}

public int numJArr(){ 
return JArr.size(); 
}

public void addJArr(Collection<J> n) { 
JArr.add(n);
} 

public Collection<K> getKArr(int index) {
return this.KArr.get(index);
}

public void setKArr(int index, Collection<K> n) {
this.KArr.add(n);
}

public int numKArr(){ 
return KArr.size(); 
}

public void addKArr(Collection<K> n) { 
KArr.add(n);
} 

public Collection<L> getLArr(int index) {
return this.LArr.get(index);
}

public void setLArr(int index, Collection<L> n) {
this.LArr.add(n);
}

public int numLArr(){ 
return LArr.size(); 
}

public void addLArr(Collection<L> n) { 
LArr.add(n);
} 

public Collection<Collection<M>> getMArr(int index) {
return this.MArr.get(index);
}

public void setMArr(int index, Collection<Collection<M>> n) {
this.MArr.add(n);
}

public int numMArr(){ 
return MArr.size(); 
}

public void addMArr(Collection<Collection<M>> n) { 
MArr.add(n);
} 

public N getNArr(int index) {
return this.NArr.get(index);
}

public void setNArr(int index, N n) {
this.NArr.add(n);
}

public int numNArr(){ 
return NArr.size(); 
}

public void addNArr(N n) { 
NArr.add(n);
} 

public Collection<Collection<Collection<O>>> getOArr(int index) {
return this.OArr.get(index);
}

public void setOArr(int index, Collection<Collection<Collection<O>>> n) {
this.OArr.add(n);
}

public int numOArr(){ 
return OArr.size(); 
}

public void addOArr(Collection<Collection<Collection<O>>> n) { 
OArr.add(n);
} 

public Collection<PPPPPPPPPPPPPP> getPPPPPPPPPPPPPPArr(int index) {
return this.PPPPPPPPPPPPPPArr.get(index);
}

public void setPPPPPPPPPPPPPPArr(int index, Collection<PPPPPPPPPPPPPP> n) {
this.PPPPPPPPPPPPPPArr.add(n);
}

public int numPPPPPPPPPPPPPPArr(){ 
return PPPPPPPPPPPPPPArr.size(); 
}

public void addPPPPPPPPPPPPPPArr(Collection<PPPPPPPPPPPPPP> n) { 
PPPPPPPPPPPPPPArr.add(n);
} 

public JSONObject toJSON() {
Map<Object, Integer> objToID = new HashMap<>();
return dfsSerializer(objToID);
}
JSONObject dfsSerializer(Map<Object, Integer> objToID) {
JSONObject jsonObject = new JSONObject();
if (objToID.containsKey(this)) {
jsonObject.put("ref", objToID.get(this));
} else {
jsonObject.put("id", objToID.size());
jsonObject.put("type", "testType");

List<JSONObject> fieldList = new ArrayList<>();
objToID.put(this, objToID.size());

JSONObject ATypeJSONObject = AType.dfsSerializer(objToID);
JSONObject ATypeJSONObjectWrapper = new JSONObject();
ATypeJSONObjectWrapper.put("AType", ATypeJSONObject);
fieldList.add(ATypeJSONObjectWrapper);

JSONObject BTypeJSONObject = BType.dfsSerializer(objToID);
JSONObject BTypeJSONObjectWrapper = new JSONObject();
BTypeJSONObjectWrapper.put("BType", BTypeJSONObject);
fieldList.add(BTypeJSONObjectWrapper);

JSONObject CTypeJSONObject = CType.dfsSerializer(objToID);
JSONObject CTypeJSONObjectWrapper = new JSONObject();
CTypeJSONObjectWrapper.put("CType", CTypeJSONObject);
fieldList.add(CTypeJSONObjectWrapper);

JSONObject DDTypeJSONObject = DDType.dfsSerializer(objToID);
JSONObject DDTypeJSONObjectWrapper = new JSONObject();
DDTypeJSONObjectWrapper.put("DDType", DDTypeJSONObject);
fieldList.add(DDTypeJSONObjectWrapper);

JSONObject EEETypeJSONObject = EEEType.dfsSerializer(objToID);
JSONObject EEETypeJSONObjectWrapper = new JSONObject();
EEETypeJSONObjectWrapper.put("EEEType", EEETypeJSONObject);
fieldList.add(EEETypeJSONObjectWrapper);

JSONObject FFTypeJSONObject = FFType.dfsSerializer(objToID);
JSONObject FFTypeJSONObjectWrapper = new JSONObject();
FFTypeJSONObjectWrapper.put("FFType", FFTypeJSONObject);
fieldList.add(FFTypeJSONObjectWrapper);

JSONObject GTypeJSONObject = GType.dfsSerializer(objToID);
JSONObject GTypeJSONObjectWrapper = new JSONObject();
GTypeJSONObjectWrapper.put("GType", GTypeJSONObject);
fieldList.add(GTypeJSONObjectWrapper);

JSONObject HTypeJSONObject = HType.dfsSerializer(objToID);
JSONObject HTypeJSONObjectWrapper = new JSONObject();
HTypeJSONObjectWrapper.put("HType", HTypeJSONObject);
fieldList.add(HTypeJSONObjectWrapper);

JSONArray IIIIIIArrJsonArray0 = new JSONArray();
for (Collection<IIIIII> l0 : IIIIIIArr) {
JSONArray IIIIIIArrJsonArray1 = new JSONArray();
for (IIIIII l1 : l0) {
IIIIIIArrJsonArray1.put(IIIIIIArrJsonArray1.length(), l1.dfsSerializer(objToID));
}
IIIIIIArrJsonArray0.put(IIIIIIArrJsonArray0.length(), IIIIIIArrJsonArray1);
}
JSONObject IIIIIIArrJSONObject = new JSONObject();
IIIIIIArrJSONObject.put("IIIIIIArr", IIIIIIArrJsonArray0);
fieldList.add(IIIIIIArrJSONObject);

JSONArray JArrJsonArray0 = new JSONArray();
for (Collection<J> l0 : JArr) {
JSONArray JArrJsonArray1 = new JSONArray();
for (J l1 : l0) {
JArrJsonArray1.put(JArrJsonArray1.length(), l1.dfsSerializer(objToID));
}
JArrJsonArray0.put(JArrJsonArray0.length(), JArrJsonArray1);
}
JSONObject JArrJSONObject = new JSONObject();
JArrJSONObject.put("JArr", JArrJsonArray0);
fieldList.add(JArrJSONObject);

JSONArray KArrJsonArray0 = new JSONArray();
for (Collection<K> l0 : KArr) {
JSONArray KArrJsonArray1 = new JSONArray();
for (K l1 : l0) {
KArrJsonArray1.put(KArrJsonArray1.length(), l1.dfsSerializer(objToID));
}
KArrJsonArray0.put(KArrJsonArray0.length(), KArrJsonArray1);
}
JSONObject KArrJSONObject = new JSONObject();
KArrJSONObject.put("KArr", KArrJsonArray0);
fieldList.add(KArrJSONObject);

JSONArray LArrJsonArray0 = new JSONArray();
for (Collection<L> l0 : LArr) {
JSONArray LArrJsonArray1 = new JSONArray();
for (L l1 : l0) {
LArrJsonArray1.put(LArrJsonArray1.length(), l1.dfsSerializer(objToID));
}
LArrJsonArray0.put(LArrJsonArray0.length(), LArrJsonArray1);
}
JSONObject LArrJSONObject = new JSONObject();
LArrJSONObject.put("LArr", LArrJsonArray0);
fieldList.add(LArrJSONObject);

JSONArray MArrJsonArray0 = new JSONArray();
for (Collection<Collection<M>> l0 : MArr) {
JSONArray MArrJsonArray1 = new JSONArray();
for (Collection<M> l1 : l0) {
JSONArray MArrJsonArray2 = new JSONArray();
for (M l2 : l1) {
MArrJsonArray2.put(MArrJsonArray2.length(), l2.dfsSerializer(objToID));
}
MArrJsonArray1.put(MArrJsonArray1.length(), MArrJsonArray2);
}
MArrJsonArray0.put(MArrJsonArray0.length(), MArrJsonArray1);
}
JSONObject MArrJSONObject = new JSONObject();
MArrJSONObject.put("MArr", MArrJsonArray0);
fieldList.add(MArrJSONObject);

JSONArray NArrJsonArray0 = new JSONArray();
for (N l0 : NArr) {
NArrJsonArray0.put(NArrJsonArray0.length(), l0.dfsSerializer(objToID));
}
JSONObject NArrJSONObject = new JSONObject();
NArrJSONObject.put("NArr", NArrJsonArray0);
fieldList.add(NArrJSONObject);

JSONArray OArrJsonArray0 = new JSONArray();
for (Collection<Collection<Collection<O>>> l0 : OArr) {
JSONArray OArrJsonArray1 = new JSONArray();
for (Collection<Collection<O>> l1 : l0) {
JSONArray OArrJsonArray2 = new JSONArray();
for (Collection<O> l2 : l1) {
JSONArray OArrJsonArray3 = new JSONArray();
for (O l3 : l2) {
OArrJsonArray3.put(OArrJsonArray3.length(), l3.dfsSerializer(objToID));
}
OArrJsonArray2.put(OArrJsonArray2.length(), OArrJsonArray3);
}
OArrJsonArray1.put(OArrJsonArray1.length(), OArrJsonArray2);
}
OArrJsonArray0.put(OArrJsonArray0.length(), OArrJsonArray1);
}
JSONObject OArrJSONObject = new JSONObject();
OArrJSONObject.put("OArr", OArrJsonArray0);
fieldList.add(OArrJSONObject);

JSONArray PPPPPPPPPPPPPPArrJsonArray0 = new JSONArray();
for (Collection<PPPPPPPPPPPPPP> l0 : PPPPPPPPPPPPPPArr) {
JSONArray PPPPPPPPPPPPPPArrJsonArray1 = new JSONArray();
for (PPPPPPPPPPPPPP l1 : l0) {
PPPPPPPPPPPPPPArrJsonArray1.put(PPPPPPPPPPPPPPArrJsonArray1.length(), l1.dfsSerializer(objToID));
}
PPPPPPPPPPPPPPArrJsonArray0.put(PPPPPPPPPPPPPPArrJsonArray0.length(), PPPPPPPPPPPPPPArrJsonArray1);
}
JSONObject PPPPPPPPPPPPPPArrJSONObject = new JSONObject();
PPPPPPPPPPPPPPArrJSONObject.put("PPPPPPPPPPPPPPArr", PPPPPPPPPPPPPPArrJsonArray0);
fieldList.add(PPPPPPPPPPPPPPArrJSONObject);

jsonObject.put("values", fieldList);
}
return jsonObject;
}
}
