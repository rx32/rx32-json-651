package mytestcase.testing.testType;

import java.util.*;
import org.json.*;

public class testType { 

public TestType() {
this.intArr = new ArrayList<>();
this.booleanArr = new ArrayList<>();
this.byteArr = new ArrayList<>();
this.floatArr = new ArrayList<>();
this.doubleArr = new ArrayList<>();
this.charArr = new ArrayList<>();
this.longArr = new ArrayList<>();
this.shortArr = new ArrayList<>();
}
private int intType;
private boolean booleanType;
private byte byteType;
private float floatType;
private double doubleType;
private char charType;
private long longType;
private short shortType;
private ArrayList<Collection<Integer>> intArr;
private ArrayList<Collection<Boolean>> booleanArr;
private ArrayList<Collection<Byte>> byteArr;
private ArrayList<Collection<Float>> floatArr;
private ArrayList<Collection<Collection<Double>>> doubleArr;
private ArrayList<Character> charArr;
private ArrayList<Collection<Collection<Collection<Long>>>> longArr;
private ArrayList<Collection<Short>> shortArr;

public int getIntType() {
return this.intType;
}

public void setIntType(int val) {
this.intType = val;
}

public boolean getBooleanType() {
return this.booleanType;
}

public void setBooleanType(boolean val) {
this.booleanType = val;
}

public byte getByteType() {
return this.byteType;
}

public void setByteType(byte val) {
this.byteType = val;
}

public float getFloatType() {
return this.floatType;
}

public void setFloatType(float val) {
this.floatType = val;
}

public double getDoubleType() {
return this.doubleType;
}

public void setDoubleType(double val) {
this.doubleType = val;
}

public char getCharType() {
return this.charType;
}

public void setCharType(char val) {
this.charType = val;
}

public long getLongType() {
return this.longType;
}

public void setLongType(long val) {
this.longType = val;
}

public short getShortType() {
return this.shortType;
}

public void setShortType(short val) {
this.shortType = val;
}

public Collection<Integer> getIntArr(int index) {
return this.intArr.get(index);
}

public void setIntArr(int index, Collection<Integer> n) {
this.intArr.add(n);
}

public int numIntArr(){ 
return intArr.size(); 
}

public void addIntArr(Collection<Integer> n) { 
intArr.add(n);
} 

public Collection<Boolean> getBooleanArr(int index) {
return this.booleanArr.get(index);
}

public void setBooleanArr(int index, Collection<Boolean> n) {
this.booleanArr.add(n);
}

public int numBooleanArr(){ 
return booleanArr.size(); 
}

public void addBooleanArr(Collection<Boolean> n) { 
booleanArr.add(n);
} 

public Collection<Byte> getByteArr(int index) {
return this.byteArr.get(index);
}

public void setByteArr(int index, Collection<Byte> n) {
this.byteArr.add(n);
}

public int numByteArr(){ 
return byteArr.size(); 
}

public void addByteArr(Collection<Byte> n) { 
byteArr.add(n);
} 

public Collection<Float> getFloatArr(int index) {
return this.floatArr.get(index);
}

public void setFloatArr(int index, Collection<Float> n) {
this.floatArr.add(n);
}

public int numFloatArr(){ 
return floatArr.size(); 
}

public void addFloatArr(Collection<Float> n) { 
floatArr.add(n);
} 

public Collection<Collection<Double>> getDoubleArr(int index) {
return this.doubleArr.get(index);
}

public void setDoubleArr(int index, Collection<Collection<Double>> n) {
this.doubleArr.add(n);
}

public int numDoubleArr(){ 
return doubleArr.size(); 
}

public void addDoubleArr(Collection<Collection<Double>> n) { 
doubleArr.add(n);
} 

public char getCharArr(int index) {
return this.charArr.get(index);
}

public void setCharArr(int index, char n) {
this.charArr.add(n);
}

public int numCharArr(){ 
return charArr.size(); 
}

public void addCharArr(char n) { 
charArr.add(n);
} 

public Collection<Collection<Collection<Long>>> getLongArr(int index) {
return this.longArr.get(index);
}

public void setLongArr(int index, Collection<Collection<Collection<Long>>> n) {
this.longArr.add(n);
}

public int numLongArr(){ 
return longArr.size(); 
}

public void addLongArr(Collection<Collection<Collection<Long>>> n) { 
longArr.add(n);
} 

public Collection<Short> getShortArr(int index) {
return this.shortArr.get(index);
}

public void setShortArr(int index, Collection<Short> n) {
this.shortArr.add(n);
}

public int numShortArr(){ 
return shortArr.size(); 
}

public void addShortArr(Collection<Short> n) { 
shortArr.add(n);
} 

public JSONObject toJSON() {
Map<Object, Integer> objToID = new HashMap<>();
return dfsSerializer(objToID);
}
JSONObject dfsSerializer(Map<Object, Integer> objToID) {
JSONObject jsonObject = new JSONObject();
if (objToID.containsKey(this)) {
jsonObject.put("ref", objToID.get(this));
} else {
jsonObject.put("id", objToID.size());
jsonObject.put("type", "testType");

List<JSONObject> fieldList = new ArrayList<>();
objToID.put(this, objToID.size());

JSONObject intTypeJSONObject = new JSONObject();
intTypeJSONObject.put("intType", intType);
fieldList.add(intTypeJSONObject);

JSONObject booleanTypeJSONObject = new JSONObject();
booleanTypeJSONObject.put("booleanType", booleanType);
fieldList.add(booleanTypeJSONObject);

JSONObject byteTypeJSONObject = new JSONObject();
byteTypeJSONObject.put("byteType", byteType);
fieldList.add(byteTypeJSONObject);

JSONObject floatTypeJSONObject = new JSONObject();
floatTypeJSONObject.put("floatType", floatType);
fieldList.add(floatTypeJSONObject);

JSONObject doubleTypeJSONObject = new JSONObject();
doubleTypeJSONObject.put("doubleType", doubleType);
fieldList.add(doubleTypeJSONObject);

JSONObject charTypeJSONObject = new JSONObject();
charTypeJSONObject.put("charType", charType);
fieldList.add(charTypeJSONObject);

JSONObject longTypeJSONObject = new JSONObject();
longTypeJSONObject.put("longType", longType);
fieldList.add(longTypeJSONObject);

JSONObject shortTypeJSONObject = new JSONObject();
shortTypeJSONObject.put("shortType", shortType);
fieldList.add(shortTypeJSONObject);

JSONArray intArrJsonArray0 = new JSONArray();
for (Collection<Integer> l0 : intArr) {
JSONArray intArrJsonArray1 = new JSONArray();
for (Integer l1 : l0) {
intArrJsonArray1.put(intArrJsonArray1.length(), l1);
}
intArrJsonArray0.put(intArrJsonArray0.length(), intArrJsonArray1);
}
JSONObject intArrJSONObject = new JSONObject();
intArrJSONObject.put("intArr", intArrJsonArray0);
fieldList.add(intArrJSONObject);

JSONArray booleanArrJsonArray0 = new JSONArray();
for (Collection<Boolean> l0 : booleanArr) {
JSONArray booleanArrJsonArray1 = new JSONArray();
for (Boolean l1 : l0) {
booleanArrJsonArray1.put(booleanArrJsonArray1.length(), l1);
}
booleanArrJsonArray0.put(booleanArrJsonArray0.length(), booleanArrJsonArray1);
}
JSONObject booleanArrJSONObject = new JSONObject();
booleanArrJSONObject.put("booleanArr", booleanArrJsonArray0);
fieldList.add(booleanArrJSONObject);

JSONArray byteArrJsonArray0 = new JSONArray();
for (Collection<Byte> l0 : byteArr) {
JSONArray byteArrJsonArray1 = new JSONArray();
for (Byte l1 : l0) {
byteArrJsonArray1.put(byteArrJsonArray1.length(), l1);
}
byteArrJsonArray0.put(byteArrJsonArray0.length(), byteArrJsonArray1);
}
JSONObject byteArrJSONObject = new JSONObject();
byteArrJSONObject.put("byteArr", byteArrJsonArray0);
fieldList.add(byteArrJSONObject);

JSONArray floatArrJsonArray0 = new JSONArray();
for (Collection<Float> l0 : floatArr) {
JSONArray floatArrJsonArray1 = new JSONArray();
for (Float l1 : l0) {
floatArrJsonArray1.put(floatArrJsonArray1.length(), l1);
}
floatArrJsonArray0.put(floatArrJsonArray0.length(), floatArrJsonArray1);
}
JSONObject floatArrJSONObject = new JSONObject();
floatArrJSONObject.put("floatArr", floatArrJsonArray0);
fieldList.add(floatArrJSONObject);

JSONArray doubleArrJsonArray0 = new JSONArray();
for (Collection<Collection<Double>> l0 : doubleArr) {
JSONArray doubleArrJsonArray1 = new JSONArray();
for (Collection<Double> l1 : l0) {
JSONArray doubleArrJsonArray2 = new JSONArray();
for (Double l2 : l1) {
doubleArrJsonArray2.put(doubleArrJsonArray2.length(), l2);
}
doubleArrJsonArray1.put(doubleArrJsonArray1.length(), doubleArrJsonArray2);
}
doubleArrJsonArray0.put(doubleArrJsonArray0.length(), doubleArrJsonArray1);
}
JSONObject doubleArrJSONObject = new JSONObject();
doubleArrJSONObject.put("doubleArr", doubleArrJsonArray0);
fieldList.add(doubleArrJSONObject);

JSONArray charArrJsonArray0 = new JSONArray();
for (Character l0 : charArr) {
charArrJsonArray0.put(charArrJsonArray0.length(), l0);
}
JSONObject charArrJSONObject = new JSONObject();
charArrJSONObject.put("charArr", charArrJsonArray0);
fieldList.add(charArrJSONObject);

JSONArray longArrJsonArray0 = new JSONArray();
for (Collection<Collection<Collection<Long>>> l0 : longArr) {
JSONArray longArrJsonArray1 = new JSONArray();
for (Collection<Collection<Long>> l1 : l0) {
JSONArray longArrJsonArray2 = new JSONArray();
for (Collection<Long> l2 : l1) {
JSONArray longArrJsonArray3 = new JSONArray();
for (Long l3 : l2) {
longArrJsonArray3.put(longArrJsonArray3.length(), l3);
}
longArrJsonArray2.put(longArrJsonArray2.length(), longArrJsonArray3);
}
longArrJsonArray1.put(longArrJsonArray1.length(), longArrJsonArray2);
}
longArrJsonArray0.put(longArrJsonArray0.length(), longArrJsonArray1);
}
JSONObject longArrJSONObject = new JSONObject();
longArrJSONObject.put("longArr", longArrJsonArray0);
fieldList.add(longArrJSONObject);

JSONArray shortArrJsonArray0 = new JSONArray();
for (Collection<Short> l0 : shortArr) {
JSONArray shortArrJsonArray1 = new JSONArray();
for (Short l1 : l0) {
shortArrJsonArray1.put(shortArrJsonArray1.length(), l1);
}
shortArrJsonArray0.put(shortArrJsonArray0.length(), shortArrJsonArray1);
}
JSONObject shortArrJSONObject = new JSONObject();
shortArrJSONObject.put("shortArr", shortArrJsonArray0);
fieldList.add(shortArrJSONObject);

jsonObject.put("values", fieldList);
}
return jsonObject;
}
}
