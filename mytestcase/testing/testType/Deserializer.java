package mytestcase.testing.testType;

import org.json.*;
import java.util.*;
public class Deserializer {

public static testType readtestType(JSONObject js) throws JSONException {
return dfstestTypeDeserializer(js, new HashMap<>());
}

private static testType dfstestTypeDeserializer(JSONObject js, Map<Integer, Object> IDToObj) {
if (js.opt("ref") == null) {
testType testType = new testType();
int id = js.getInt("id");
JSONArray fieldList = js.getJSONArray("values");
IDToObj.put(id, testType);

testType.setIntType((int)(fieldList.getJSONObject(0).get("intType")));

testType.setBooleanType((Boolean)(fieldList.getJSONObject(1).get("booleanType")));

testType.setByteType((byte)(int)(fieldList.getJSONObject(2).get("byteType")));

testType.setFloatType((Float)(fieldList.getJSONObject(3).get("floatType")));

testType.setDoubleType((Double)(fieldList.getJSONObject(4).get("doubleType")));

testType.setCharType((char)(int)(fieldList.getJSONObject(5).get("charType")));

testType.setLongType((Long)(fieldList.getJSONObject(6).get("longType")));

testType.setShortType((short)(int)(fieldList.getJSONObject(7).get("shortType")));

for (Object l0 : fieldList.getJSONObject(8).getJSONArray("intArr")) {
Collection<Integer> c1 = new ArrayList<>();
for (Object l1 : (JSONArray)l0) {
c1.add((Integer)l1);
}
testType.addIntArr(c1);
}
for (Object l0 : fieldList.getJSONObject(9).getJSONArray("booleanArr")) {
Collection<Boolean> c1 = new ArrayList<>();
for (Object l1 : (JSONArray)l0) {
c1.add((Boolean)l1);
}
testType.addBooleanArr(c1);
}
for (Object l0 : fieldList.getJSONObject(10).getJSONArray("byteArr")) {
Collection<Byte> c1 = new ArrayList<>();
for (Object l1 : (JSONArray)l0) {
c1.add((Byte)l1);
}
testType.addByteArr(c1);
}
for (Object l0 : fieldList.getJSONObject(11).getJSONArray("floatArr")) {
Collection<Float> c1 = new ArrayList<>();
for (Object l1 : (JSONArray)l0) {
c1.add((Float)l1);
}
testType.addFloatArr(c1);
}
for (Object l0 : fieldList.getJSONObject(12).getJSONArray("doubleArr")) {
Collection<Collection<Double>> c1 = new ArrayList<>();
for (Object l1 : (JSONArray)l0) {
Collection<Double> c2 = new ArrayList<>();
for (Object l2 : (JSONArray)l1) {
c2.add((Double)l2);
}
c1.add(c2);
}
testType.addDoubleArr(c1);
}
for (Object l0 : fieldList.getJSONObject(13).getJSONArray("charArr")) {
testType.addCharArr((Character)l0);
}
for (Object l0 : fieldList.getJSONObject(14).getJSONArray("longArr")) {
Collection<Collection<Collection<Long>>> c1 = new ArrayList<>();
for (Object l1 : (JSONArray)l0) {
Collection<Collection<Long>> c2 = new ArrayList<>();
for (Object l2 : (JSONArray)l1) {
Collection<Long> c3 = new ArrayList<>();
for (Object l3 : (JSONArray)l2) {
c3.add((Long)l3);
}
c2.add(c3);
}
c1.add(c2);
}
testType.addLongArr(c1);
}
for (Object l0 : fieldList.getJSONObject(15).getJSONArray("shortArr")) {
Collection<Short> c1 = new ArrayList<>();
for (Object l1 : (JSONArray)l0) {
c1.add((Short)l1);
}
testType.addShortArr(c1);
}
return testType;
} else {
return (testType) IDToObj.get(js.getInt("ref"));
}
}
}

